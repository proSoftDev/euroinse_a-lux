<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.02.2019
 * Time: 11:45
 */

namespace app\controllers;


use app\models\Aboutsubtitle;
use app\models\Bankdetails;
use app\models\Contact;
use app\models\Contracta;
use app\models\Contractb;
use app\models\Contractc;
use app\models\Document;
use app\models\ProductSubcategory;
use app\models\ProductSubcategoryFao;
use app\models\ProductCategory;
use app\models\Fao;
use app\models\Glossary;
use app\models\Information;
use app\models\Insurancea;
use app\models\Insuranceb;
use app\models\License;
use app\models\Manual;
use app\models\Menu;
use app\models\Productcapital;
use app\models\Productcorp;
use app\models\Productfamily;
use app\models\Product;
use app\models\Supportsubtitle;
use app\models\Typesofreport;
use app\models\Vacancy;
use Yii;
use yii\web\Controller;

class SearchController extends Controller
{
    public function init()
    {
        $menu = Menu::find()->asArray()->all();
        $contact = Contact::find()->asArray()->all();
        $subAbout = Aboutsubtitle::find()->asArray()->all();
        $subProduct = Product::find()->asArray()->all();
        $subSupport = Supportsubtitle::find()->asArray()->all();
        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['contact'] = $contact;
        Yii::$app->view->params['subAbout'] = $subAbout;
        Yii::$app->view->params['subProduct'] = $subProduct;
        Yii::$app->view->params['subSupport'] = $subSupport;
    }
    public function actionIndex($text)
    {
        $search = $text;
        if($text){
            $contracta = Contracta::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();
            $contractb = Contractb::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();
            $contractc = Contractc::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();
            $aboutInf = Information::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();
            $aboutRep = Typesofreport::find()->where("content LIKE '%$text%'")->all();
            $aboutBank = Bankdetails::find()->where("title LIKE '%$text%' OR subtitle LIKE '%$text%'")->all();
            $aboutVacancy = Vacancy::find()->where("name LIKE '%$text%'")->all();
            $aboutManual = Manual::find()->where("fullname LIKE '%$text%' OR position LIKE '%$text%'")->all();
            $aboutLicense = License::find()->where("title LIKE '%$text%'")->all();
            $productEnsure = ProductSubcategory::find()->where("title LIKE '%$text%' OR content LIKE '%$text%'")->all();
            $supportFao = Fao::find()->where("title LIKE '%$text%'")->all();
            $supportGlossary = Glossary::find()->where("title LIKE '%$text%'")->all();
            $supportDoc = Document::find()->where("title LIKE '%$text%'")->with('documentItems')->all();
            $ensA = Insurancea::find()->where("name LIKE '%$text%'")->all();
            $ensB = Insuranceb::find()->where("name LIKE '%$text%'")->all();

            $count = count($contracta) + count($contractb) + count($contractc) + count($aboutInf) + count($aboutRep)
                + count($aboutBank)+ count($aboutVacancy)+ count($aboutManual)+ count($aboutLicense)+count($productCorp)
                + count($productFamily)+count($productCapital)+count($productEnsure)+count($supportFao)+count($supportGlossary)
                + count($supportDoc)+count($ensA)+count($ensB);
        }else{
            $count = 0;
            return $this->render('index', compact('search', 'count', 'text'));
        }

        return $this->render('index',compact('count','contracta','contractb','contractc','aboutInf',
            'aboutRep','aboutBank','search','aboutVacancy','aboutManual','aboutLicense','productCorp','productFamily',
            'productCapital','productEnsure','supportFao','supportGlossary','supportDoc','ensA','ensB'));
    }


    public static function cutStr($str, $length=50, $postfix='...')
    {
        if ( strlen($str) <= $length)
            return $str;

        $temp = substr($str, 0, $length);
        return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
    }

}