<?php

namespace app\controllers;

use app\models\Aboutsubtitle;
use app\models\Bankdetails;
use app\models\Contact;
use app\models\Contracta;
use app\models\Contractb;
use app\models\Contractc;
use app\models\Contractsubtitle;
use app\models\Document;
use app\models\Fao;
use app\models\Glossary;
use app\models\Infdoc;
use app\models\Information;
use app\models\Insurance;
use app\models\Insurancea;
use app\models\Insurancealist;
use app\models\Insurancealistdoc;
use app\models\Insuranceatypedoc;
use app\models\Insuranceb;
use app\models\Insurancec;
use app\models\Insuranceccontent;
use app\models\Insurancesubtitle;
use app\models\License;
use app\models\Manualsubtitle;
use app\models\Menu;
use app\models\Pay;
use app\models\PayForm;
use app\models\PayInformation;
use app\models\Product;
use app\models\Report;
use app\models\Supportsubtitle;
use app\models\Typesofreport;
use app\models\Vacancy;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{


    public function init()
    {
        $menu = Menu::find()->asArray()->all();
        $contact = Contact::find()->asArray()->all();
        $subAbout = Aboutsubtitle::find()->asArray()->all();
        $products = Product::find()->all();
        $subSupport = Supportsubtitle::find()->asArray()->all();
        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['contact'] = $contact;
        Yii::$app->view->params['subAbout'] = $subAbout;
        Yii::$app->view->params['products'] = $products;
        Yii::$app->view->params['subSupport'] = $subSupport;
		
		if(!isset($_SESSION['isActive'])) $_SESSION['isActive'] = 1;
		
				
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $menus = Menu::find()->all();
        return $this->render('index', compact('menus'));
    }

    /**.
     *
     * Login action
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */


    public function actionAbout($activeBlock)
    {

        $licenses = License::find()->all();
        $information = Information::find()->asArray()->all();
        $docInf = Infdoc::find()->all();
        $about = Aboutsubtitle::find()->asArray()->all();
        $manual = Manualsubtitle::find()->with('manualItems')->all();
        $reports = Report::find()->all();

        $reportInf = Typesofreport::getReportInformation(6);
        $reportManual = Typesofreport::getReportManual(6);
        $reportLicense = Typesofreport::getReportLicense(6);
        $reportText = Typesofreport::getReportText(6);

        $bankDetails = Bankdetails::find()->all();
        $vacancies = Vacancy::getAll(3);


        return $this->render('about',
            compact('licenses', 'information', 'about', 'manual',
                'reports', 'reportInf', 'reportLicense', 'reportManual', 'bankDetails', 'vacancies', 'activeBlock', 'docInf', 'reportText'));
    }

    public function actionPay()
    {
        $pay = Pay::find()->one();
        return $this->render('pay', compact('pay'));
    }

    public function actionPayInformation()
    {
        $payInfo = PayInformation::find()->all();
        return $this->render('pay-information', compact('payInfo'));
    }

    public function actionSupport($activeBlock)
    {
        $contact = Contact::find()->asArray()->all();
        $faos = Fao::find()->asArray()->all();
        $glossaries = Glossary::find()->asArray()->all();
        $title = Supportsubtitle::find()->asArray()->all();

        $documents = Document::find()->with('documentItems')->all();

        return $this->render('support', compact('contact', 'faos', 'glossaries', 'title', 'documents', 'activeBlock'));
    }



    public function actionEnsure()
    {
        $ensTitle = Insurance::find()->all();
        $ensSub = Insurancesubtitle::find()->all();
        $ensB = Insuranceb::find()->all();

        $ensC = Insurancec::find()->all();
        $ensCa = Insuranceccontent::find()->with('documentItems')->where('insurancec_id = 1')->all();
        $ensCb = Insuranceccontent::find()->with('documentItems')->where('insurancec_id = 2')->all();

        $ensA = Insurancea::find()->all();
        $ensAList = Insurancealist::find()->with('items')->all();
        $ensAListDoc = Insurancealistdoc::find()->all();
        $ensATypeDoc = Insuranceatypedoc::find()->all();
//        die(debug($ensATypeDoc));

        return $this->render('ensure', compact('ensTitle', 'ensSub', 'ensB', 'ensC', 'ensCa', 'ensCb', 'ensA',
            'ensAList', 'ensAListDoc', 'ensATypeDoc'));
    }

    public function actionChangeEnsure($activeBlock)
    {
        $contracts["contracta"] = Contracta::find()->asArray()->all();
        $contracts["contractb"] = Contractb::find()->asArray()->all();
        $contracts["contractc"] = Contractc::find()->asArray()->all();
        $contracts["title"] = Contractsubtitle::find()->asArray()->all();

        return $this->render('change-ensure', compact("contracts", 'activeBlock'));

    }



    public function actionPayForm()
    {

        $model = new PayForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                return 1;

            } else {

                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
    }

    public function actionPayFormDb()
    {
        $model = new PayForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
            $model->status = 1;
            if ($model->save()) {
                return 1;
            } else {

                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
        else{
            return 21312312;
        }

    }

    public function actionPayDbFalse()
    {
        $model = new PayForm();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->get())) {
            if ($model->save()) {
                return 1;
            } else {

                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }
        else{
            return 21312312;
        }

    }


}
