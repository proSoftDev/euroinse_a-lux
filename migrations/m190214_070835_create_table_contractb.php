<?php

use yii\db\Migration;

/**
 * Class m190214_070835_create_table_contractb
 */
class m190214_070835_create_table_contractb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contractb', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'subtitle' => $this->string(255),
            'content' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_070835_create_table_contractb cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_070835_create_table_contractb cannot be reverted.\n";

        return false;
    }
    */
}
