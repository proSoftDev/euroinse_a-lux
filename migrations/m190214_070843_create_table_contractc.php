<?php

use yii\db\Migration;

/**
 * Class m190214_070843_create_table_contractc
 */
class m190214_070843_create_table_contractc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('contractc', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'subtitle' => $this->string(255),
            'content' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190214_070843_create_table_contractc cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190214_070843_create_table_contractc cannot be reverted.\n";

        return false;
    }
    */
}
