<?php

use yii\db\Migration;

/**
 * Class m190217_160510_create_table_manual
 */
class m190217_160510_create_table_manual extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('manual', [
            'id' => $this->primaryKey(),
            'fullname' => $this->string(255),
            'position' => $this->string(255),
            'experience' => $this->string(255),
            'image' => $this->string(255)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190217_160510_create_table_manual cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190217_160510_create_table_manual cannot be reverted.\n";

        return false;
    }
    */
}
