<?php

use yii\db\Migration;

/**
 * Class m190218_082632_create_table_vacancy
 */
class m190218_082632_create_table_vacancy extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('vacancy', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'content' => $this->text(),
            'price' => $this->string(255),
            'address' => $this->string(255),
            'date' => $this->date(),
            'employment' => $this->string(255)
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('vacancy');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190218_082632_create_table_vacancy cannot be reverted.\n";

        return false;
    }
    */
}
