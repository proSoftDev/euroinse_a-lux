<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bankdetails".
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $rnn
 * @property string $bin
 * @property string $iik
 * @property string $bank
 */
class Bankdetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bankdetails'.Yii::$app->session["lang"];;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'subtitle', 'rnn', 'bin', 'iik', 'bank'], 'required'],
            [['title', 'subtitle', 'rnn', 'bin', 'iik', 'bank'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'rnn' => 'РНН',
            'bin' => 'БИН ',
            'iik' => 'ИИК',
            'bank' => 'БАНК',
        ];
    }
}
