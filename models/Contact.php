<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $telephoneNumber
 * @property string $whatsapNumber
 * @property string $faxNumber
 * @property string $email
 * @property string $webSite
 * @property string $prospect
 * @property string $ugol
 * @property string $workTime
 * @property string $text1
 * @property string $text2
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telephoneNumber', 'whatsapNumber', 'faxNumber', 'email', 'webSite', 'prospect', 'ugol', 'workTime', 'text1', 'text2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephoneNumber' => 'Телефон',
            'whatsapNumber' => 'WhatsApp',
            'faxNumber' => 'Факс',
            'email' => 'E-mail',
            'webSite' => 'Web сайт',
            'prospect' => 'Проспект',
            'ugol' => 'Угол, улица',
            'workTime' => 'Рабочее время',
            'text1' => 'Текст',
            'text2' => 'Текст',
        ];
    }
}
