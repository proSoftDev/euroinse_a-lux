<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contracta".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 */
class Contracta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contracta'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title','content'],'required'],
            [['content'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Загаловок',
            'content' => 'содержание',
        ];
    }
}
