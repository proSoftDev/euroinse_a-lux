<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contractc".
 *
 * @property int $id
 * @property string $title

 * @property string $content
 */
class Contractc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contractc'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Загаловок',
            'content' => 'содержание',
        ];
    }
}
