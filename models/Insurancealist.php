<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancealist".
 *
 * @property int $id
 * @property string $name
 */
class Insurancealist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancealist'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Insurancealist::find()->all(),'id','name');
    }

    public function getItems(){
        return $this->hasMany(Insuranceatype::className(), ['insurancealist_id' => 'id']);
    }

    public function getDocument(){
        return $this->hasMany(Insurancealistdoc::className(), ['insurancealist_id' => 'id']);
    }
}
