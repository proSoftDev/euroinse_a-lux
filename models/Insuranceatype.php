<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insuranceatype".
 *
 * @property int $id
 * @property string $name
 * @property int $insurancealist_id
 */
class Insuranceatype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insuranceatype'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'insurancealist_id'], 'required'],
            [['insurancealist_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'insurancealist_id' => ' Категории',
        ];
    }

    public function getItem()
    {
        return $this->hasOne(Insurancealist::className(), ['id' => 'insurancealist_id']);
    }

    public function getItemName(){
        return (isset($this->item))? $this->item->name:'Не задан';
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Insuranceatype::find()->all(),'id','name');
    }

}
