<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insuranceatypedoc".
 *
 * @property int $id
 * @property string $file
 * @property int $insuranceatype_id
 */
class Insuranceatypedoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insuranceatypedoc'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file', 'insuranceatype_id'], 'required'],
            [['insuranceatype_id'], 'integer'],
            [['file'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'PDF Файл',
            'insuranceatype_id' => 'Подкатегории списка',
        ];
    }


    public function getDocument()
    {
        return $this->hasOne(Insuranceatype::className(), ['id' => 'insuranceatype_id']);
    }

    public function getDocumentName(){
        return (isset($this->document))? $this->document->name:'Не задан';
    }

    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }
}
