<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancec".
 *
 * @property int $id
 * @property string $title
 */
class Insurancec extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancec'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Insurancec::find()->all(),'id','title');
    }

    public function getInsurancecItems(){
        return $this->hasMany(Insuranceccontent::className(), ['insurancec_id' => 'id']);
    }
}
