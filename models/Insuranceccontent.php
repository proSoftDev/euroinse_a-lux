<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insuranceccontent".
 *
 * @property int $id
 * @property string $name
 * @property int $insurancec_id
 */
class Insuranceccontent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insuranceccontent'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'insurancec_id'], 'required'],
            [['insurancec_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'insurancec_id' => ' Виды',
        ];
    }

    public function getContent()
    {
        return $this->hasOne(Insurancec::className(), ['id' => 'insurancec_id']);
    }

    public function getContentName(){
        return (isset($this->content))? $this->content->title:'Не задан';
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Insuranceccontent::find()->all(),'id','name');
    }


    public function getDocumentItems(){
        return $this->hasMany(Insurancecdocument::className(), ['insuranceccontent_id' => 'id']);
    }


}
