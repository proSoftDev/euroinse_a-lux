<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "insurancesubtitle".
 *
 * @property int $id
 * @property string $subtitle
 */
class Insurancesubtitle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurancesubtitle'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subtitle'], 'required'],
            [['subtitle'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subtitle' => 'Категории',
        ];
    }
}
