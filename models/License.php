<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "license".
 *
 * @property int $id
 * @property string $title
 * @property string $image
 * @property string $file
 * @property string $text
 */
class License extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'license'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['title','text'], 'string', 'max' => 255],
            [['file'],'file','extensions'=>'pdf'],
            [['image'],'file','extensions'=>'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Названия лицензия ',
            'image' => 'Изображение',
            'file' => 'Файл',
            'text' => 'Названия файла',
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }
}
