<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manualsubtitle".
 *
 * @property int $id
 * @property string $text
 */
class Manualsubtitle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manualsubtitle'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Заголовок',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Manualsubtitle::find()->all(),'id','text');
    }

    public function getManualItems(){
        return $this->hasMany(Manual::className(), ['manualsubtitle_id' => 'id']);
    }
}
