<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay".
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $pholder1
 * @property string $pholder2
 * @property string $pholder3
 * @property string $pholder4
 * @property string $pholder5
 * @property string $agree
 * @property string $buttonName
 */
class Pay extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'subtitle', 'pholder1', 'pholder2', 'pholder3', 'pholder4', 'pholder5', 'agree', 'buttonName'], 'required'],
            [['title', 'subtitle', 'pholder1', 'pholder2', 'pholder3', 'pholder4', 'pholder5', 'agree', 'buttonName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'pholder1' => 'Текст договора',
            'pholder2' => 'Текст ИИН',
            'pholder3' => 'Текст карта',
            'pholder4' => 'Текст сумма',
            'pholder5' => 'Текст телефона',
            'agree' => 'Текст согласие',
            'buttonName' => 'Названия кнопка',
        ];
    }
}
