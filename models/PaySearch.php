<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pay;

/**
 * PaySearch represents the model behind the search form of `app\models\Pay`.
 */
class PaySearch extends Pay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title', 'subtitle', 'pholder1', 'pholder2', 'pholder3', 'pholder4', 'pholder5', 'agree', 'buttonName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pay::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'subtitle', $this->subtitle])
            ->andFilterWhere(['like', 'pholder1', $this->pholder1])
            ->andFilterWhere(['like', 'pholder2', $this->pholder2])
            ->andFilterWhere(['like', 'pholder3', $this->pholder3])
            ->andFilterWhere(['like', 'pholder4', $this->pholder4])
            ->andFilterWhere(['like', 'pholder5', $this->pholder5])
            ->andFilterWhere(['like', 'agree', $this->agree])
            ->andFilterWhere(['like', 'buttonName', $this->buttonName]);

        return $dataProvider;
    }
}
