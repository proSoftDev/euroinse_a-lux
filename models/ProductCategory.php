<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ProductCategory".
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $product_id
 * @property string $created_at
 * @property string $updated_at
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_categories'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'product_id'], 'required'],
            [['subtitle'], 'string'],
            [['product_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'product_id' => 'Продукт',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования'
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductName(){
        return (isset($this->product))? $this->product->title : 'Не задано';
    }

    public static function getList(){
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }

    public function getSubcategories(){
        return $this->hasMany(ProductSubcategory::className(), ['product_category_id' => 'id']);
    }



}
