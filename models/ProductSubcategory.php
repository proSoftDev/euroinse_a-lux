<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProductSubcategory".
 *
 * @property int $id
 * @property string $tarif
 * @property string $title
 * @property string $content
 * @property int $product_category_id
 */
class ProductSubcategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_subcategories'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'product_category_id'], 'required'],
            [['content'], 'string'],
            [['product_category_id'], 'integer'],
            [['tarif', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tarif' => 'Тариф',
            'title' => 'Заголовка',
            'content' => 'Содержание',
            'product_category_id' => 'Категория продукта',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'product_category_id']);
    }

    public function getCategoryName(){
        return (isset($this->category))? $this->category->title : 'Не задано';
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(self::find()->all(),'id','title');
    }


    public function getFao(){
        return $this->hasMany(ProductSubcategoryFao::className(), ['product_subcategory_id' => 'id']);
    }

}
