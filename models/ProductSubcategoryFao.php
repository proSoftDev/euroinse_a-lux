<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ProductSubcategoryFao".
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property int $product_subcategory_id
 */
class ProductSubcategoryFao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_subcategory_fao'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'content', 'product_subcategory_id'], 'required'],
            [['content'], 'string'],
            [['product_subcategory_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'content' => 'Содержание',
            'product_subcategory_id' => 'Контент',
        ];
    }


    public function getProductSubcategory()
    {
        return $this->hasOne(ProductSubcategory::className(), ['id' => 'product_subcategory_id']);
    }

    public function getProductSubcategoryTitle(){
        return (isset($this->productSubcategory)) ? $this->productSubcategory->title : 'Не задано';
    }



}
