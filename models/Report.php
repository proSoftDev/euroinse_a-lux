<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property int $id
 * @property string $title
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Названия',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(\app\models\Report::find()->all(),'id','title');
    }

    public function getReportItems(){
        return $this->hasMany(TypesofReport::className(), ['report_id' => 'id']);
    }
}
