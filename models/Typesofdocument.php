<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "typesofdocument".
 *
 * @property int $id
 * @property string $name
 * @property string $file
 * @property int $document_id
 */
class Typesofdocument extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'typesofdocument'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'document_id'], 'required'],
            [['id', 'document_id'], 'integer'],
            [['name'], 'string'],
            [['file'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['file'], 'file', 'extensions' => 'pdf']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Текст',
            'file' => 'PDF Файл',
            'document_id' => 'Формы',
        ];
    }

    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    public function getDocumentName(){
        return (isset($this->document))? $this->document->title:'Не задан';
    }

    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }




}
