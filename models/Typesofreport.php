<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "typesofreport".
 *
 * @property int $id
 * @property string $content
 * @property string $file
 * @property int $report_id
 */
class Typesofreport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'typesofreport'.Yii::$app->session["lang"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'report_id'], 'required'],
            [['content'], 'string'],
            [['report_id'], 'integer'],
            [['file'], 'file', 'extensions' => 'pdf,xls']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержания',
            'file' => 'Файл',
            'report_id' => 'Категория',
        ];
    }



    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }

    public function getReportName(){
        return (isset($this->report))? $this->report->title:'Не задан';
    }



    public function saveFile($filename)
    {
        $this->file = $filename;
        return $this->save(false);
    }

    public function getFile()
    {
        return ($this->file) ? '/uploads/pdf/' . $this->file : '';
    }

    public static function getAll($pageSize, $report_id)
    {

        Yii::$app->view->params['about'] = "report";
        $query = Typesofreport::find()->where('report_id ='.$report_id)->orderBy('id desc');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $data = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $data['data'] = $data;
        $data['pagination'] = $pagination;

        return $data;
    }


    public static function getReportInformation($pageSize = 6)
    {
        return  Typesofreport::getAll($pageSize, 1);
    }

    public static function getReportManual($pageSize = 6)
    {
        return  Typesofreport::getAll($pageSize, 2);
    }

    public static function getReportLicense($pageSize = 6)
    {
        return  Typesofreport::getAll($pageSize, 3);
    }


    public static function getReportText($pageSize = 6)
    {
        return  Typesofreport::getAll($pageSize, 4);
    }


}
