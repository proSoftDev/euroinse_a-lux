<?php

namespace app\modules\admin\controllers;

use app\models\Contracta;
use app\models\Contractb;
use app\models\Contractc;
use app\models\LoginForm;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{


    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect("/admin/menu/index");
    }
}
