<?php

namespace app\modules\admin\controllers;

use app\models\FileUpload;
use Yii;
use app\models\Insurancealistdoc;
use app\models\InsurancealistdocSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InsurancealistdocController implements the CRUD actions for Insurancealistdoc model.
 */
class InsurancealistdocController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Insurancealistdoc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InsurancealistdocSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Insurancealistdoc model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Insurancealistdoc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Insurancealistdoc();

        $upload = new FileUpload();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if($file == null){
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                $file->saveAs($upload->getFolder() . $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Insurancealistdoc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
            $upload = new FileUpload();
            $oldFileName = $model->file;

            if ($model->load(Yii::$app->request->post())) {
                $file = UploadedFile::getInstance($model, 'file');
                if($file == null){
                    $model->file = $oldFileName;
                    $model->save();
                }else{
                    $file->saveAs($upload->getFolder() . $file->baseName . '.' . $file->extension);
                    $model->file = $file->baseName . '.' . $file->extension;
                    $model->save(false);

                    if(!($oldFileName == null)){
                        unlink($upload->getFolder() . $oldFileName);
                    }
                    return $this->redirect(['view', 'id' => $model->id]);

                }
                return $this->redirect(['view', 'id' => $model->id]);

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Insurancealistdoc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Insurancealistdoc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Insurancealistdoc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Insurancealistdoc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
