<?php

namespace app\modules\admin\controllers;

use app\models\FileUpload;
use Yii;
use app\models\Insuranceatypedoc;
use app\models\InsuranceatypedocSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InsuranceatypedocController implements the CRUD actions for Insuranceatypedoc model.
 */
class InsuranceatypedocController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Insuranceatypedoc models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InsuranceatypedocSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Insuranceatypedoc model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Insuranceatypedoc model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Insuranceatypedoc();

        $upload = new FileUpload();

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if($file == null){
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
                $file->saveAs($upload->getFolder() . $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Insuranceatypedoc model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $upload = new FileUpload();
        $oldFileName = $model->file;

        if ($model->load(Yii::$app->request->post())) {
            $file = UploadedFile::getInstance($model, 'file');
            if($file == null){
                $model->file = $oldFileName;
            }else{
                $file->saveAs($upload->getFolder() . $file->baseName . '.' . $file->extension);
                $model->file = $file->baseName . '.' . $file->extension;

                if($oldFileName != null && file_exists($upload->getFolder() . $oldFileName)){
                    unlink($upload->getFolder() . $oldFileName);
                }
				
            }
			
			if($model->save()){
            	return $this->redirect(['view', 'id' => $model->id]);
			}

        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Insuranceatypedoc model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Insuranceatypedoc model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Insuranceatypedoc the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Insuranceatypedoc::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
