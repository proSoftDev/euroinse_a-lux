<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aboutsubtitle */

$this->title = 'Create Aboutsubtitle';
$this->params['breadcrumbs'][] = ['label' => 'Aboutsubtitles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aboutsubtitle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
