<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bankdetails */

$this->title = 'Создать банковские реквизиты';
$this->params['breadcrumbs'][] = ['label' => 'Bankdetails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bankdetails-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
