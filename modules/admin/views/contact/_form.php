<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telephoneNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'whatsapNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'faxNumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'webSite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prospect')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ugol')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'workTime')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'text1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text2')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
