<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContactSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'telephoneNumber') ?>

    <?= $form->field($model, 'whatsapNumber') ?>

    <?= $form->field($model, 'faxNumber') ?>

    <?= $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'webSite') ?>

    <?php // echo $form->field($model, 'prospect') ?>

    <?php // echo $form->field($model, 'ugol') ?>

    <?php // echo $form->field($model, 'workTime') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
