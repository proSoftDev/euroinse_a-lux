<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractc */

$this->title = 'Create Contractc';
$this->params['breadcrumbs'][] = ['label' => 'Contractcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contractc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
