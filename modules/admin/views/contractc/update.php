<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractc */

$this->title = 'Update Contractc: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Contractcs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contractc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
