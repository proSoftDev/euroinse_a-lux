<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsubtitle */

$this->title = 'Create Contractsubtitle';
$this->params['breadcrumbs'][] = ['label' => 'Contractsubtitles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contractsubtitle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
