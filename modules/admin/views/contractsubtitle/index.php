<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContractsubtitleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подкатегории договоров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contractsubtitle-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Contractsubtitle', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{update} {view}'],
        ],
    ]); ?>
</div>
