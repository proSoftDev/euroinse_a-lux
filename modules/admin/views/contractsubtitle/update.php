<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contractsubtitle */

$this->title = 'Редактирование подкатегории договоров: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Contractsubtitles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contractsubtitle-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
