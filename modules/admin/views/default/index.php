<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главный меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">
    <h1>
        Админка
    </h1>
    <p style="font-size: 60px;margin-top: 100px;margin-left: 100px;">Здравствуйте, Администратор!</p>
</div>