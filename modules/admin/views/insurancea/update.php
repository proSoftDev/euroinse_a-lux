<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insurancea */

$this->title = 'Update Insurancea: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Insuranceas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insurancea-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
