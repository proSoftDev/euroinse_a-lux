<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insurancealist */

$this->title = 'Создании  категории';
$this->params['breadcrumbs'][] = ['label' => 'Insurancealists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancealist-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
