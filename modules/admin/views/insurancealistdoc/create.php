<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insurancealistdoc */

$this->title = 'Создании документа для категорий';
$this->params['breadcrumbs'][] = ['label' => 'Insurancealistdocs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancealistdoc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
