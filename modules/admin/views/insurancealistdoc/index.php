<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsurancealistdocSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Документы для категорий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancealistdoc-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать документ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'file',
            ['attribute'=>'insurancealist_id', 'value'=>function($model){ return $model->documentName;},'filter'=>\app\models\Insurancealist::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
