<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsuranceatypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Подкатегории списка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insuranceatype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать подкатегории списка', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            ['attribute'=>'insurancealist_id', 'value'=>function($model){ return $model->itemName;},'filter'=>\app\models\Insurancealist::getList()],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
