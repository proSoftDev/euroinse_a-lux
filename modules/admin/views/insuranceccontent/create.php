<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insuranceccontent */

$this->title = 'Создании категории';
$this->params['breadcrumbs'][] = ['label' => 'Insuranceccontents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insuranceccontent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
