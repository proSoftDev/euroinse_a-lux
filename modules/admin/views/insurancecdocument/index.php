<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsurancecdocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '  Документы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurancecdocument-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать документы', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
//            'file',
            ['attribute'=>'insuranceccontent_id', 'value'=>function($model){ return $model->contentName;},'filter'=>\app\models\Insuranceccontent::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
