<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Insurancecdocument */

$this->title = 'Редактировании документа: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Insurancecdocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="insurancecdocument-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
