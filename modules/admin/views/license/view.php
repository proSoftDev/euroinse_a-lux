<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\License */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Licenses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="license-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $filelink = $model->getFile();?>
    <?php if($filelink != null) $file = ['attribute'=>'file','value' => Html::a('Посмотреть pdf файл',$filelink,['target'=>'_blank']),'format' => 'raw' ];
    else $file = "file"?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
             [
                'attribute'=>'image',
                'value'=>$model->getImage(),
                'format' => ['image',['width'=>'200','height'=>'280']],
            ],
            $file,
            'text',
        ],
    ]) ?>

</div>
