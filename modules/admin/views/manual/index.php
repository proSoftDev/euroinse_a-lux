<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ManualSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Руководство';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manual-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fullname',
            ['attribute'=>'manualsubtitle_id', 'value'=>function($model){ return $model->manualName;},'filter'=>\app\models\Manualsubtitle::getList()],
//            'position',
//            'experience',
//            'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
