<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PayFormSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Данные об оплате';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Pay Form', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'number_contract',
//            'iin',
            'sum',
            'phone',
            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\controllers\LabelPay::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
