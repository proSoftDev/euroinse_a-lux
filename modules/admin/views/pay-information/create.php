<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PayInformation */

$this->title = 'Create Pay Information';
$this->params['breadcrumbs'][] = ['label' => 'Pay Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-information-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
