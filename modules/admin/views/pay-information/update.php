<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PayInformation */

$this->title = 'Редактировать информацию: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Информация об оплате', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="pay-information-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
