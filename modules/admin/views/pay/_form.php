<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subtitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pholder1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pholder2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pholder3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pholder4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pholder5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'agree')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'buttonName')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
