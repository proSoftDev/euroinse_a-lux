<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PaySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pay-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'subtitle') ?>

    <?= $form->field($model, 'pholder1') ?>

    <?= $form->field($model, 'pholder2') ?>

    <?php // echo $form->field($model, 'pholder3') ?>

    <?php // echo $form->field($model, 'pholder4') ?>

    <?php // echo $form->field($model, 'pholder5') ?>

    <?php // echo $form->field($model, 'agree') ?>

    <?php // echo $form->field($model, 'buttonName') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
