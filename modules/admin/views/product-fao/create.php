<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductFao */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Выпадающий список продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-fao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
