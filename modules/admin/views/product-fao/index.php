<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductFaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Выпадающий список продуктов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-fao-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'attribute'=>'product_id',
                'filter'=>\app\models\Product::getListWhereNoCategory(),
                'value'=>function($model){
                    return $model->getProductName();
                },
            ],
            'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
