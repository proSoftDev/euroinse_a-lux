<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSubcategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ensureborrower-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tarif')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content' )->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'full',    // basic, standard, full
            'inline' => false,      //по умолчанию false
        ])
    ]); ?>


    <?= $form->field($model, 'product_category_id')->dropDownList(\app\models\ProductCategory::getList()) ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
