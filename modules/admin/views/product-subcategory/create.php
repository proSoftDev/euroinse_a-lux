<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductSubcategory */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => ' Подкатегории продукта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ensureborrower-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
