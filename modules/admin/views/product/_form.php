<?php

use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<div class="productsubtitle-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_status')->dropDownList(\app\modules\admin\status\Label::statusList()) ?>

    <?= $form->field($model, 'image')->fileInput() ?>

    <?= $form->field($model, 'content' )->widget(CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
            'preset' => 'full',    // basic, standard, full
            'inline' => false,      //по умолчанию false
        ])
    ]); ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>

    if($('#product-category_status').val() == 1){
        $('.field-product-content').closest('div').hide();
    }else{
        $('.field-product-content').closest('div').show();
    }

    $('#product-category_status').on('change', function () {
        if($('#product-category_status').val() == 1){
            $('.field-product-content').closest('div').hide();
        }else{
            $('.field-product-content').closest('div').show();
        }
    });

</script>
