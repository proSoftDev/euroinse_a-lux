<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Typesofdocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="typesofdocument-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'document_id')->dropDownList(\app\models\Document::getList()) ?>

    <?= $form->field($model, 'file')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
