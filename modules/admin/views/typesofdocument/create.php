<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Typesofdocument */

$this->title = 'Создать содержании формов';
$this->params['breadcrumbs'][] = ['label' => 'Typesofdocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="typesofdocument-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
