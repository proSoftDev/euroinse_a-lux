<?php

use app\models\Typesofdocument;
use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel app\models\TypesofdocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Содержания форма';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="typesofdocument-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать содержания форма', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name:ntext',
//            'file',
            ['attribute'=>'document_id', 'value'=>function($model){ return $model->documentName;},'filter'=>\app\models\Document::getList()],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
