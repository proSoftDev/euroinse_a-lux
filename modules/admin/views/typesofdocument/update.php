<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Typesofdocument */

$this->title = 'Редактировании содержании формов: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Typesofdocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="typesofdocument-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
