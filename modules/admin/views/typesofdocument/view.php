<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Typesofdocument */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Typesofdocuments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="typesofdocument-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php $filelink = $model->getFile();?>
    <?php if($filelink != null) $file = ['attribute'=>'file','value' => Html::a('Посмотреть pdf файл',$filelink,['target'=>'_blank']),'format' => 'raw' ];
    else $file = "file"?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'format' => 'raw',
                'attribute' => 'name',
                'value' => function($data){
                    return $data->name;
                }
            ],
            $file,
            ['attribute'=>'document_id', 'value'=>function($model){ return $model->documentName;}],
        ],
    ]) ?>

</div>
