<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Typesofreport */

$this->title = 'Create Typesofreport';
$this->params['breadcrumbs'][] = ['label' => 'Typesofreports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="typesofreport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
