<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TypesofreportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Содержании отчетов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="typesofreport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать содержании отчетов', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'content:ntext',
            ['attribute'=>'report_id', 'value'=>function($model){ return $model->reportName;},'filter'=>\app\models\Report::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
