

<?php if(Yii::$app->session->get('lang') == "_kz"):   ?>
<div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle" style="color:#26287f !important" >Уважаемые клиенты!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style = "color:#26287f !important">


				<p>Төтенше жағдай жөніндегі Мемлекеттік комиссияның шешіміне байланысты біз 2020 жылдың 30 наурызынан 11 мамырына дейін қашықтық жұмысқа ауысуға міндеттіміз.</p>
				<p>Құжаттарды қабылдау электронды түрде жүзеге асырылады. Электрондық құжаттар нысандары және оларды қабылдау әдістері:</p>
				
				<p>- сақтандыру келісімшарттары бойынша: <br> Өтініш нысаны: 
					<a href="http://www.euroins.kz/site/change-ensure?activeBlock=changeEnsA" target="_blank">
						http://www.euroins.kz/site/change-ensure?activeBlock=changeEnsA</a> <br>
					Қабылдау әдісі:  <a href="mailto:info@euroins.kz">info@euroins.kz</a>
				</p>
				
				<p>
					- сақтандыру жағдайлары бойынша: <br>
					Нұсқаулық және құжаттар нысандары: <a href="http://www.euroins.kz/site/ensure"        															target="_blank">http://www.euroins.kz/site/ensure</a> <br>
					Қабылдау әдісі: WhatsApp <a href="https://api.whatsapp.com/send?phone=+77010532642&amp;text=" target="_blank">+ 7-701-053-26-42</a>; <a href="mailto:info@euroins.kz">info@euroins.kz</a>

				</p>
				
				<p>
					Жинақтаушы сақтандыру келісімшартын төлеу осы жерде жүзеге асырылады: <br>
					<a href="http://www.euroins.kz/site/pay">http://www.euroins.kz/site/pay</a>
				</p>

				<p>Байланыс орталығына жүктің жоғарылауына байланысты, Сізден жоғарыда көрсетілген байланыс әдістерін қолдануды сұраймыз.</p>

				<p style="text-align: center;">
			Түсінікпен қарауыңызды өтінеміз!
Компанияның қызметіндегі барлық өзгерістер туралы қосымша хабарланады.


				</p>
			
			</div>

		</div>
	</div>
</div>

<?php else:?>

<div class="modal fade" id="modalWarning" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle" style="color:#26287f !important" >Уважаемые клиенты!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style = "color:#26287f !important">


				<p>В связи с решением Государственной комиссии по обеспечению режима чрезвычайного положения мы обязаны перейти на дистанционную работу с 30 марта по 11 мая 2020 года.</p>
				
				<p> Прием документов осуществляется в электронном виде.
Электронные формы документов и способы их приема следующие:</p>
				
				<p>- по договору страхования: <br>
					Форма заявления: <a href="site/change-ensure?activeBlock=changeEnsA" target="_blank">http://www.euroins.kz/site/change-ensure?	activeBlock=changeEnsA</a></p>
				
				<p>Способ приема:  <a href="mailto:info@euroins.kz">info@euroins.kz</a></p>
				
				<p>- по страховым случаям  <br> 
					Инструкция и формы документов: <a href="http://www.euroins.kz/site/ensure" targer="_blank">http://www.euroins.kz/site/ensure</a>  <br>
					Способ приема: WhatsApp <a href="https://api.whatsapp.com/send?phone=+77010532642&amp;text=" target="_blank">+ 7-701-053-26-42</a>; 
					<a href="mailto:info@euroins.kz">info@euroins.kz</a>
				</p>
					
				<p>
					Оплата договора накопительного страхования производится здесь: <br> <a href="http://www.euroins.kz/site/pay">http://www.euroins.kz/site/pay</a>
				</p>
				
				<p>
					В связи с повышенной нагрузкой на Колл Центр, просим Вас использовать вышеуказанные  методы коммуникации.
				</p>
				
				<p style="text-align: center;">
					Просим отнестись с пониманием! <br>
Обо всех возможных изменениях в деятельности Компании будет сообщено дополнительно. 

				</p>
			</div>

		</div>
	</div>
</div>

<?php endif;?>

  






