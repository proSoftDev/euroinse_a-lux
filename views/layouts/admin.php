<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AdminAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute(['menu/index'])?>" class="logo">
            <span class="logo-mini"><b>A</b>LT</span>
            <span class="logo-lg">Админ. Панель</span>
        </a>
<!--        --><?//= Html::a('<span class="logo-mini"></span><span class="logo-lg">Админ. панель</span>', ['default/index'], ['class' => 'logo']) ?>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Админ">
                            <!-- The user image in the navbar-->
                            <img src="/private/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">Alexander Pierce</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="/private/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                                <p>
                                    Alexander Pierce - Administrator
                                    <small>2017-<?=date('Y');?></small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/admin/admin-profile/index" class="btn btn-default btn-flat">Профиль</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Выйти</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <select class="language-button selectpicker" onchange="location = this.options[this.selectedIndex].value;">
                        <option <?=(Yii::$app->session["lang"]=='')?'selected':''?> value="/lang/index?url=ru">RU</option>
                        <option <?=(Yii::$app->session["lang"]=='_kz')?'selected':''?> value="/lang/index?url=kz">KZ</option>
                    </select>

                </ul>
            </div>
        </nav>
    </header>
    <? $contracta = $this->params['contracta'][0]['title'];?>
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/private/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Alexander Pierce</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>

            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">ОСНОВНАЯ НАВИГАЦИЯ</li>
            </ul>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                    'items' => [

                        ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['/admin/menu/index']],
                        [
                            'label' => 'Изменение договора',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Подкатегории договоров', 'icon' => 'fa fa-user', 'url' => ['/admin/contractsubtitle'], 'active' => $this->context->id == 'contractsubtitle'],
                                ['label' => 'Страхование заемщиков', 'icon' => 'fa fa-user', 'url' => ['/admin/contracta'], 'active' => $this->context->id == 'contracta'],
                                ['label' => 'Защита Семьи', 'icon' => 'fa fa-user', 'url' => ['/admin/contractb'], 'active' => $this->context->id == 'contractb'],
                                ['label' => 'Накопительное страхование', 'icon' => 'fa fa-user', 'url' => ['/admin/contractc'], 'active' => $this->context->id == 'contractc'],
                            ],
                        ],
                        [
                            'label' => 'Клиентская поддержка',
                            'icon' => 'fa fa-home',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Подкатегории поддержки', 'icon' => 'fa fa-user', 'url' => ['/admin/supportsubtitle'], 'active' => $this->context->id == 'supportsubtitle'],
                                ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['/admin/contact'], 'active' => $this->context->id == 'contact'],
                                ['label' => 'FAO', 'icon' => 'fa fa-user', 'url' => ['/admin/fao'], 'active' => $this->context->id == 'fao'],
                                ['label' => 'Словарь терминов', 'icon' => 'fa fa-user', 'url' => ['/admin/glossary'], 'active' => $this->context->id == 'glossary'],
//                                ['label' => 'Формы документов', 'icon' => 'fa fa-user', 'url' => ['/admin/document/index']],
                                [
                                    'label' => 'Формы документов',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Формы', 'icon' => 'fa fa-user', 'url' => ['/admin/document'], 'active' => $this->context->id == 'document'],
                                        ['label' => 'Содержание', 'icon' => 'fa fa-user', 'url' => ['/admin/typesofdocument'], 'active' => $this->context->id == 'typesofdocument'],

                                    ],
                                ],
                            ],
                        ],
                        [
                            'label' => 'О компании',
                            'icon' => 'fa fa-home',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Подкатегории о компании', 'icon' => 'fa fa-user', 'url' => ['/admin/aboutsubtitle'], 'active' => $this->context->id == 'aboutsubtitle'],
                                [
                                    'label' => 'Общая информация',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Cодержание', 'icon' => 'fa fa-user', 'url' => ['/admin/information'], 'active' => $this->context->id == 'information'],
                                        ['label' => 'Документы', 'icon' => 'fa fa-user', 'url' => ['/admin/infdoc'], 'active' => $this->context->id == 'infdoc'],

                                    ],
                                ],
                                [
                                    'label' => 'Руководство',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Подкатегории руководство', 'icon' => 'fa fa-user', 'url' => ['/admin/manualsubtitle'], 'active' => $this->context->id == 'manualsubtitle'],
                                        ['label' => 'Содержании руководство', 'icon' => 'fa fa-user', 'url' => ['/admin/manual'], 'active' => $this->context->id == 'manual'],

                                    ],
                                ],
                                ['label' => 'Лицензия', 'icon' => 'fa fa-user', 'url' => ['/admin/license'], 'active' => $this->context->id == 'license'],
                                [
                                    'label' => 'Отчет',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Подкатегории отчетов', 'icon' => 'fa fa-user', 'url' => ['/admin/report'], 'active' => $this->context->id == 'report'],
                                        ['label' => 'Содержании отчетов', 'icon' => 'fa fa-user', 'url' => ['/admin/typesofreport'], 'active' => $this->context->id == 'typesofreport'],

                                    ],
                                ],
                                ['label' => 'Банковские реквизиты', 'icon' => 'fa fa-user', 'url' => ['/admin/bankdetails'], 'active' => $this->context->id == 'bankdetails'],
                                ['label' => 'Вакансии', 'icon' => 'fa fa-user', 'url' => ['/admin/vacancy'], 'active' => $this->context->id == 'vacancy'],

                            ],
                        ],

                        [
                            'label' => 'Продукты',
                            'icon' => 'fa fa-home',
                            'url' => '#',
                            'items' => [

                                ['label' => 'Продукты', 'icon' => 'fa fa-user', 'url' => ['/admin/product'], 'active' => $this->context->id == 'product'],
                                ['label' => 'Вы. список продуктов', 'icon' => 'fa fa-user', 'url' => ['/admin/product-fao'], 'active' => $this->context->id == 'product-fao'],
                                ['label' => 'Категории продукта', 'icon' => 'fa fa-user', 'url' => ['/admin/product-category'], 'active' => $this->context->id == 'product-category'],
                                ['label' => 'Подкатегории продукта', 'icon' => 'fa fa-user', 'url' => ['/admin/product-subcategory'], 'active' => $this->context->id == 'product-subcategory'],
                                ['label' => 'Вы. список категорий', 'icon' => 'fa fa-user', 'url' => ['/admin/product-subcategory-fao'], 'active' => $this->context->id == 'product-subcategory-fao'],
                            ],
                        ],


                        [
                            'label' => 'Страховой случай',
                            'icon' => 'fa fa-user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Заголовок', 'icon' => 'fa fa-user', 'url' => ['/admin/insurance'], 'active' => $this->context->id == 'insurance'],
                                ['label' => 'Подкатегории', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancesubtitle'], 'active' => $this->context->id == 'insurancesubtitle'],
                                [
                                    'label' => 'При наступлении',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Этапы', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancea'], 'active' => $this->context->id == 'insurancea'],
                                        [
                                            'label' => 'Собрать документы',
                                            'icon' => 'fa fa-home',
                                            'url' => '#',
                                            'items' => [
                                                ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancealist'], 'active' => $this->context->id == 'insurancealist'],
                                                ['label' => 'Подкатегории списка', 'icon' => 'fa fa-user', 'url' => ['/admin/insuranceatype'], 'active' => $this->context->id == 'insuranceatype'],
                                                ['label' => 'Для категорий', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancealistdoc'], 'active' => $this->context->id == 'insurancealistdoc'],

                                                ['label' => 'Для подкатегорий', 'icon' => 'fa fa-user', 'url' => ['/admin/insuranceatypedoc'], 'active' => $this->context->id == 'insuranceatypedoc'],
                                            ],
                                        ],


                                    ],
                                ],
                                ['label' => 'Действия компании', 'icon' => 'fa fa-user', 'url' => ['/admin/insuranceb'], 'active' => $this->context->id == 'insuranceb'],
                                [
                                    'label' => 'Условия страхования',
                                    'icon' => 'fa fa-home',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Виды', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancec'], 'active' => $this->context->id == 'insurancec'],
                                        ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['/admin/insuranceccontent'], 'active' => $this->context->id == 'insuranceccontent'],
                                        ['label' => 'Документы', 'icon' => 'fa fa-user', 'url' => ['/admin/insurancecdocument'], 'active' => $this->context->id == 'insurancecdocument'],
                                    ],
                                ],
                            ],
                        ],
                        ['label' => 'Оплата', 'icon' => 'fa fa-user', 'url' => ['/admin/pay'], 'active' => $this->context->id == 'pay'],
                        ['label' => 'Данные об оплате', 'icon' => 'fa fa-user', 'url' => ['/admin/pay-form'], 'active' => $this->context->id == 'pay-form'],
                        ['label' => 'Информация об оплате', 'icon' => 'fa fa-user', 'url' => ['/admin/pay-information'], 'active' => $this->context->id == 'pay-information'],
                    ],
                ]
            ) ?>
        </section>
    </aside>



    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?=$content?>

        </section>
    </div>

    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.4.0
        </div>
        <strong>Copyright &copy; 2007-<?=date('Y');?>
            <a target="_blank" href="https://a-lux.kz">A-lux</a>.</strong> Все права защищены.
    </footer>


    <div class="control-sidebar-bg"></div>
</div>

<?php $this->endBody() ?>
<?php $this->registerJsFile('/ckeditor/ckeditor.js');?>
<?php $this->registerJsFile('/ckfinder/ckfinder.js');?>
<script>
    $(document).ready(function(){
        var editor = CKEDITOR.replaceAll();
        CKFinder.setupCKEditor( editor );
    })
    $.widget.bridge('uibutton', $.ui.button);
</script>
</body>
</html>
<?php $this->endPage() ?>
