<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use app\controllers\SiteController;

PublicAsset::register($this);

$productLinks [] = '/product';
foreach (\app\models\Product::find()->all() as $v){
    $productLinks[] = '/product/view?id='.$v->id;
}

$aboutChangLink = substr($_SERVER['REQUEST_URI'], count($_SERVER['REQUEST_URI'])-11,10);
$aboutLinks = array('/site/about?activeBlock=inf','/site/about?activeBlock=man','/site/about?activeBlock=lic','/site/about?activeBlock=rep','/site/about?activeBlock=bank','/site/about?activeBlock=job');
$supportLinks = array('/site/support?activeBlock=cont','/site/support?activeBlock=fao','/site/support?activeBlock=glos','/site/support?activeBlock=doc');


if($_SERVER['REQUEST_URI']== '/site/index'){ $title = $this->params['menu'][0]["metaName"];$keywords=$this->params['menu'][0]["metaKeyword"];$description=$this->params['menu'][0]["metaDescription"];}
elseif(in_array($_SERVER['REQUEST_URI'], $aboutLinks) || $aboutChangLink == 'per-page=3' || $aboutChangLink == 'per-page=6'){ $title = $this->params['menu'][1]["metaName"];$keywords=$this->params['menu'][1]["metaKeyword"];$description=$this->params['menu'][1]["metaDescription"];}
elseif(in_array($_SERVER['REQUEST_URI'], $productLinks)){ $title = $this->params['menu'][2]["metaName"];$keywords=$this->params['menu'][2]["metaKeyword"];$description=$this->params['menu'][2]["metaDescription"];}
elseif(in_array($_SERVER['REQUEST_URI'], $supportLinks)){ $title = $this->params['menu'][3]["metaName"];$keywords=$this->params['menu'][3]["metaKeyword"];$description=$this->params['menu'][3]["metaDescription"];}
elseif($_SERVER['REQUEST_URI']== '/site/ensure'){ $title = $this->params['menu'][4]["metaName"];$keywords=$this->params['menu'][4]["metaKeyword"];$description=$this->params['menu'][4]["metaDescription"];}
elseif($_SERVER['REQUEST_URI']== '/site/pay'){ $title = $this->params['menu'][5]["metaName"];$keywords=$this->params['menu'][5]["metaKeyword"];$description=$this->params['menu'][5]["metaDescription"];}

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-140921182-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-140921182-1');
	</script>
	<title><?=$title?></title>
	<link rel="shortcut icon" href="/public/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/public/images/favicon.ico" type="image/x-icon">
    <meta name="Description" content="<?=$description?>">
    <meta name="Keywords" content="<?=$keywords?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
	
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
		
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(58107112, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/58107112" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	
<?=$this->render('_modal')?>
	
<div class="main" id="main">
    <div id="european-main-pay" class="european-main-pay">

        <div class="hamburger">
            <a href="#" class="hamburger-item">
                <span class="hamburger-inner"></span>
            </a>
        </div>

        <div class="menu-wrap">

            <div class="menu-logo wow fadeInDown">
                <a href="<?=Url::toRoute("/site/index")?>"><img src="/public/images/logo.png" alt="European Insurance Company"></a>
            </div>

            <div class="menu-langs wow fadeInLeft">
                <div class="menu-langs-item">
                    <a href="<?=Url::toRoute(['/lang/index','url'=>'ru'])?>">
                        <img src="/public/images/flag-ru.png" alt="ru">
                        <span>ru</span>
                    </a>
                </div>
                <div class="menu-langs-item">
                    <a href="<?=Url::toRoute(["/lang/index", 'url' => 'kz'])?>">
                        <img src="/public/images/flag-kz.png" alt="kz">
                        <span>kz</span>
                    </a>
                </div>


            </div>


            <form action="/search" method="get" class="menu-search wow fadeInLeft">
                <a href="">
                <img  name="text"  src="/public/images/menu-loupe.png" alt="Поиск">
                </a>
                 <?php if(Yii::$app->session["lang"] == ''):?>
                    <input  name="text" placeholder="Поиск..." type="text">
                <?php endif;?>
                <?php if(Yii::$app->session["lang"] == '_kz'):?>
                    <input  name="text"  placeholder="Іздеу..." type="text">
                <?php endif;?>
<!--                <button style="display:none;" id="search_action"></button>-->

            </form>

            <nav class="menu-link-wrap wow fadeInUp">
                <ul>
                    <?php if($this->params['menu'][0]["status"] == 1):?>
                        <li class="<?=($_SERVER['REQUEST_URI']== '/site/index' || $_SERVER['REQUEST_URI']== '/') ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['site/index'])?>"><?=$this->params['menu'][0]["title"]?></a></li>
                    <?php endif;?>
                    <?php if($this->params['menu'][1]["status"] == 1):?>
                        <li class="<?=(in_array($_SERVER['REQUEST_URI'], $aboutLinks) || $aboutChangLink == 'per-page=3' || $aboutChangLink == 'per-page=6') ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['site/about','activeBlock'=>'inf'])?>"><?=$this->params['menu'][1]["title"]?></a></li>
                    <?php endif;?>
                    <?php if($this->params['menu'][2]["status"] == 1):?>
                        <li class="<?=(in_array($_SERVER['REQUEST_URI'], $productLinks)) ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['/product'])?>"><?=$this->params['menu'][2]["title"]?></a></li>
                    <?php endif;?>
                    <?php if($this->params['menu'][3]["status"] == 1):?>
                        <li class="<?=(in_array($_SERVER['REQUEST_URI'], $supportLinks)) ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['site/support','activeBlock'=>'cont'])?>"><?=$this->params['menu'][3]["title"]?></a></li>
                    <?php endif;?>
                    <?php if($this->params['menu'][4]["status"] == 1):?>
                        <li class="<?=($_SERVER['REQUEST_URI']== '/site/ensure') ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['site/ensure'])?>"><?=$this->params['menu'][4]["title"]?></a></li>
                    <?php endif;?>
                    <?php if($this->params['menu'][5]["status"] == 1):?>
                        <li class="<?=($_SERVER['REQUEST_URI']== '/site/pay') ? 'menu-link-active' : ''?>"><a href="<?= Url::toRoute(['site/pay'])?>"><?=$this->params['menu'][5]["title"]?></a></li>
                    <?php endif;?>
                </ul>
                
            </nav>

            <div class="menu-phone-number">
                <img src="/public/images/menu-phone-number.png" class="wow shake" alt="Телефон">
                <a href="tel:<?=$this->params['contact'][0]["telephoneNumber"]?>"><?=$this->params['contact'][0]["telephoneNumber"]?></a>
            </div>

            <div class="menu-copyright">
                <p>© 2018 European Insurance Company Все права защищены.</p>
            </div>

        </div>

        <?=$content?>


        <footer class="footer" id="footer">

            <!-- СТРЕЛКА ВВЕРХ -->
            <div class="arrow-up wow rotateInUpLeft">
                <a href="#main">
                    <img src="/public/images/arrow-up.png" alt="Вверх">
                </a>
            </div>
            <!-- END СТРЕЛКА ВВЕРХ -->

            <div class="footer-content">

               
                    <div class="foooter-about-company footer-item wow fadeInUp">
                        <h3><?=$this->params['menu'][1]["title"]?></h3>
                        <ul>
							<li><a href="<?=URL::toRoute(['site/index'])?>"><?=$this->params['menu'][0]["title"]?></a></li>
							 <?php if($this->params['menu'][1]["status"] == 1):?>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'inf'])?>"><?=$this->params['subAbout'][0]["title"]?></a></li>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'man'])?>"><?=$this->params['subAbout'][1]["title"]?></a></li>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'lic'])?>"><?=$this->params['subAbout'][2]["title"]?></a></li>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'rep'])?>"><?=$this->params['subAbout'][3]["title"]?></a></li>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'bank'])?>"><?=$this->params['subAbout'][4]["title"]?></a></li>
                            <li><a href="<?=URL::toRoute(['site/about', 'activeBlock'=>'job'])?>"><?=$this->params['subAbout'][5]["title"]?></a></li>
							 <?php endif;?>
                        </ul>
                    </div>
               
                <?php if($this->params['menu'][2]["status"] == 1):?>
                    <div class="foooter-products footer-item wow fadeInUp" data-wow-delay="0.1s">
                        <h3><?=$this->params['menu'][2]["title"]?></h3>
                        <ul>
                            <? foreach ($this->params['products'] as $product):?>
                                <li><a href="<?=URL::toRoute(['product/view', 'id' => $product->id])?>"><?=$product->title;?></a></li>
                            <? endforeach;?>
                        </ul>
                        <?php if($this->params['menu'][4]["status"] == 1):?>
                            <a href="<?=URL::toRoute('site/ensure')?>"><h3 class="space-top2 sm-hide"><?=$this->params['menu'][4]["title"]?></h3></a>
                        <?php endif;?>
                    </div>
                <?php endif;?>
                <?php if($this->params['menu'][3]["status"] == 1):?>
                <div class="foooter-support footer-item wow fadeInUp" data-wow-delay="0.2s">
                    <h3><?=$this->params['menu'][3]["title"]?></h3>
                    <ul>
                        <li><a href="<?=URL::toRoute(['site/support', 'activeBlock'=>'cont'])?>"><?=$this->params['subSupport'][0]["title"]?></a></li>
                        <li><a href="<?=URL::toRoute(['site/support', 'activeBlock'=>'fao'])?>"><?=$this->params['subSupport'][1]["title"]?></a></li>
                        <li><a href="<?=URL::toRoute(['site/support', 'activeBlock'=>'glos'])?>"><?=$this->params['subSupport'][2]["title"]?></a></li>
                        <li><a href="<?=URL::toRoute(['site/support', 'activeBlock'=>'doc'])?>"><?=$this->params['subSupport'][3]["title"]?></a></li>
                       <? if(Yii::$app->session["lang"] == '_kz'):?>
						<li><a style="color:#26287f;" href="https://www.euroins.kz/site/change-ensure?activeBlock=changeEnsA">Сақтандыру шартын өзгерту</a></li>
						<? else:?>
						<li><a style="color:#26287f;" href="https://www.euroins.kz/site/change-ensure?activeBlock=changeEnsA">Изменение договора страхования</a></li>
						<? endif;?>
                    </ul>
                    <?php if($this->params['menu'][5]["status"] == 1):?>
                        <a href="<?=URL::toRoute('site/pay')?>"><h3 class="space-top2 sm-hide"><?=$this->params['menu'][5]["title"]?></h3></a>
                    <?php endif;?>
                </div>
                <?php endif;?>


                <div class="foooter-support footer-item wow fadeInUp" data-wow-delay="0.3s">
                    <h3><?=$this->params['menu'][7]["title"]?></h3>

                    <ul>
                        <?php if($this->params['contact'][0]["telephoneNumber"] != null): ?>
                            <li><span><img src="/public/images/footer-phone.png" alt="Телефон"></span><a href="tel:<?=$this->params['contact'][0]["telephoneNumber"]?>"><?=$this->params['contact'][0]["telephoneNumber"]?></a></li>
                        <?php endif;?>
                        
                        <?php if($this->params['contact'][0]["email"] != null): ?>
                            <li><span><img src="/public/images/footer-message.png" alt="Сообщение"></span><a href="mailto:<?=$this->params['contact'][0]["email"]?>"><?=$this->params['contact'][0]["email"]?></a></li>
                        <?php endif;?>
                        <?php if($this->params['contact'][0]["prospect"] != null && $this->params['contact'][0]["ugol"] != null): ?>
                            <li><span><img src="/public/images/footer-location.png" alt="Локация"></span><a href="#"><?=$this->params['contact'][0]["prospect"]?>
                                    <br ><span style="margin-left: 30px;"><?=$this->params['contact'][0]["ugol"]?></span></a></li>
                        <?php endif;?>
                        <?php if($this->params['contact'][0]["workTime"] != null): ?>
                        <li><span><img src="/public/images/footer-clock.png" alt="График работы"></span><a href="#">График работы: <?=$this->params['contact'][0]["workTime"]?></a></li>
                        <?php endif;?>
                        <li><span><img src="/public/images/footer-map.png" alt="Карта сайта"></span><a href="#">Карта сайта</a></li>
                    </ul>
                </div>
				<div class="super-lg foooter-support footer-item wow fadeInUp" data-wow-delay="0.4s">               

                    <ul>
						<?php if($this->params['menu'][4]["status"] == 1):?>
                            <a href="<?=URL::toRoute('site/ensure')?>"><h3 class=""><?=$this->params['menu'][4]["title"]?></h3></a>
                        <?php endif;?>
                       <?php if($this->params['menu'][5]["status"] == 1):?>
                        <a href="<?=URL::toRoute('site/pay')?>"><h3 class=""><?=$this->params['menu'][5]["title"]?></h3></a>
                    <?php endif;?>
						

                    </ul>
                </div>
            </div>

        </footer>
		
		

        <div class="copyright wow fadeInLeft">
            <p>© 2018 European Insurance Company   Все права защищены.</p>
        </div>

        <div class="dev-alux">
            <a href="https://www.a-lux.kz/" target="_blank">Разработано в<img src="/public/images/alux-dark.png" class="img-fluid" alt=""></a>
        </div>


<?php $this->endBody() ?>

<?php /* if($_SERVER['REQUEST_URI'] == '/'): ?>
	<?php $_SESSION['isActive'] = 0;?>
	<script>
		$("#modalWarning").modal("show");
	</script>
<?php endif; */?>
		
</body>
</html>
	
<?php $this->endPage() ?>
