

<nav aria-label="page navigation example" >
    <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination' => $pagination,
            'options' => ['class' => 'pagination wow fadeInUp'],
            'linkOptions' => ['class' => 'page-link'],
            'firstPageLabel' => 'В начало',
            'firstPageCssClass' => 'page-link',
            'prevPageLabel' => 'Назад',
            'prevPageCssClass' => 'page-link',
            'lastPageLabel' => 'В конец',
            'lastPageCssClass' => 'page-link',
            'nextPageLabel' => 'Вперёд',
            'nextPageCssClass' => 'page-link',
            'activePageCssClass' => 'active1' ,
            'hideOnSinglePage' => true,
        ]);
    ?>
</nav>

<!--<nav aria-label="page navigation example" >-->
<!---->
<!--    <ul class="pagination wow fadeInUp" data-wow-delay="1s" >-->
<!--        <li  class="page-link a"><a href="#">В начало</a></li>-->
<!--        <li ><a  href="#">Назад</a></li>-->
<!--        <li ><a class="page-link active1" href="#">1</a></li>-->
<!--        <li ><a class="page-link" href="#">2</a></li>-->
<!--        <li ><a class="page-link" href="#">3</a></li>-->
<!--        <li ><a class="page-link" href="#">4</a></li>-->
<!--        <li ><a class="page-link" href="#">5</a></li>-->
<!--        <li ><a class="page-link" href="#">Вперёд</a></li>-->
<!--        <li ><a class="page-link" href="#">В конец</a></li>-->
<!--    </ul>-->
<!--</nav>-->