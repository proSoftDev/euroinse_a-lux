<?php

/* @var $this yii\web\View */


use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="european-products-wrap">

    <? $m=0;?>
    <? foreach ($products as $product):?>
        <? $m++;?>
        <? if($m % 2 == 1):?>
        <div class="european-products-item-wrap">
        <? endif;?>

            <? if($m % 4 == 1):?>
                <? $class = 'borrowers-insurance wow slideInDown';?>
            <? elseif($m % 4 == 2):?>
                <? $class = 'corporate-insurance wow slideInRight';?>
            <? elseif($m % 4 == 3):?>
                <? $class = 'family-protect wow slideInLeft';?>
            <? else:?>
                <? $class = 'savings-insurance wow slideInUp';?>
            <? endif;?>
            <div class="european-products-item <?=$class;?>" style='background-image: url("<?=$product->getImage();?>")'>
                <a href="<?=URL::toRoute(['product/view', 'id' => $product->id]);?>">
                    <?=$product->title;?>
                </a>
            </div>

        <? if($m % 2 == 0):?>
        </div>
        <? endif;?>
    <? endforeach;?>

    <? if($m % 2 != 0):?>
    </div>
    <? endif;?>

</div>


</div>
</div>
