<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Product-ensure';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="product-ensure-content">
    <div class="tabs">
        <ul class="nav nav-tabs ">
            <? foreach ($products as $v):?>
                <li class="nav-item ">
                    <a class="nav-link line-hover <?=$v->id == $product->id ? "active" : "";?>"  href="<?=URL::toRoute(['product/view', 'id' => $v->id]);?>">
                        <?=$v->title;?>
                    </a>
                </li>
            <? endforeach;?>
        </ul>
        <ul class="nav nav-tabs ">
            <? $m = 0;?>
            <? foreach ($categories as $v):?>
                <? $m++;?>
                <li class="nav-item">
                    <a class="nav-link <?= $m == 1 ? "active" : "";?>" data-toggle="tab" href="#tab<?=$v->id;?>">
                        <?=$v->title;?></a>
                </li>
            <? endforeach;?>
        </ul>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <? if($product->category_status):?>
            <? $n = 0;?>
            <? foreach ($categories as $v):?>
                <? $n++;?>
                <div class="tab-pane <?= $n == 1 ? "active" : "";?>" id="tab<?=$v['id']?>">

                    <div class="tabs">
                        <?if($v->subtitle != null):?>
                            <?=$v->subtitle;?>
                        <? endif;?>
                        <ul class="nav nav-tabs ">
                            <? $m=0;?>
                            <? if($v->subcategories != null):?>
                                <? foreach ($v->subcategories as $item):?>
                                    <? if($item->tarif != null):?>
                                        <?php $m++;?>
                                        <?php if($m == 1) $active = 'active';
                                        else $active = '';?>
                                        <li class="nav-item">
                                            <a class="nav-link line-hover <?=$active;?>" data-toggle="tab" href="#full<?=$item->id;?>">
                                                <?=$item->tarif;?>
                                            </a>
                                        </li>
                                    <? endif;?>
                                <? endforeach;?>
                            <? endif;?>
                        </ul>
                    </div>

                    <div class="tab-content">

                        <?php $m = 0;?>
                        <? if($v->subcategories):?>
                            <? foreach ($v->subcategories as $item):?>
                                <? $m++;?>
                                <? if($m == 1) $active = 'active';
                                else $active = '';?>
                                <div class="tab-pane <?=$active?>" id="full<?=$item->id;?>">
                                    <h4><?=$item->title;?></h4>
                                    <div class="separator"></div>
                                    <div class="inner-title product-ensure-text wow fadeInUp">
                                        <?=$item->content;?>
                                    </div>

                                    <?php $m = 0;?>
                                    <? foreach ($item->fao as $list):?>
                                        <? $m++;?>
                                        <?if($m < 4):?>
                                            <div class="qa-block">
                                                <div class="qa-section" href="#collapseExample<?=$list->id;?>" data-toggle="collapse">
                                                    <?=$list->title;?>
                                                    <img src="/public/images/plus-ico.png" alt="">
                                                </div>
                                                <div class="collapse" id="collapseExample<?=$list->id;?>">
                                                    <div class="product-inner-text">
                                                        <?=$list->content;?>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif;?>
                                    <? endforeach;?>


                                    <?if(count($item->fao) >= 4):?>
                                        <div class="more">
                                            <p href="#collapse-all" data-toggle="collapse">Показать еще <img src="/public/images/more-ico.png" alt=""></p>
                                        </div>
                                        <div class="collapse" id="collapse-all">
                                            <?php $m = 0;?>
                                            <? foreach ($item->fao as $list):?>
                                                <?if($m >= 4):?>
                                                    <div class="qa-block">
                                                        <div class="qa-section" href="#collapseExample<?=$list->id;?>" data-toggle="collapse">
                                                            <?=$list->title;?>
                                                            <img src="/public/images/plus-ico.png" alt="">
                                                        </div>
                                                        <div class="collapse" id="collapseExample<?=$list->id;?>">
                                                            <div class="product-inner-text">
                                                                <?=$list->content;?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif;?>
                                            <? endforeach;?>
                                        </div>
                                    <? endif;?>

                                </div>
                            <? endforeach;?>
                        <? endif;?>
                    </div>
                </div>
            <? endforeach;?>
        <? else:?>
            <div class="tab-pane active" id="6">
                <div class="licence">

                    <div class="inner-title wow fadeInUp">
                        <h4><?=$product->title;?></h4>
                    </div>
                    <div class="separator"></div>
                    <div class="product-ensure-text wow fadeInUp">
                        <?=$product->content;?>
                    </div>

                    <? if($product->fao != null):?>
                        <?php foreach ($product->fao as $v): ?>
                            <div class="qa-block">
                                <div class="qa-section" href="#collapseExample<?=$v['id']?>" data-toggle="collapse">
                                    <?=$v->title;?>
                                    <img src="/public/images/plus-ico.png" alt="">
                                </div>
                                <div class="collapse" id="collapseExample<?=$v['id']?>">
                                    <div class="product-inner-text">
                                        <?=$v->content;?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    <? endif;?>
                </div>
            </div>
    <? endif;?>
    </div>
</div>
<!-- End Оплата ошибка -->



</div>
</div>
</div>

