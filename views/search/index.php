<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 22.02.2019
 * Time: 11:48
 */

use yii\helpers\Url;

$this->title = 'Поиск';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="european-pay-content" style='background-image: url("/public/images/about-bg.jpg");'>

    <div class="european-pay-title wow slideInLeft">
        <?if($count){?>
            <div class="" style="font-size: 22px;margin-bottom: 20px;">Результаты поиска по запросу <span style="font-weight: bold;"><?=$search?></span>.</div>
        <?}else{?>
            <div class="" style="font-size: 22px;margin-bottom: 20px;">По запросу <span style="font-weight: bold;"><?=$search?></span> ничего не найдено.</div>
        <?}?>

    </div>

    <?if($count){?>
    <div class="european-pay-form-wrap">
        <?foreach($contracta as $v){?>
            <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
                <div class="main__section1__block2">
                    <h3><?=$v->title?></h3>
                    <?=\app\controllers\SearchController::cutStr($v->content, 800)?>
                    <a href="<?=URL::toRoute(['site/change-ensure','activeBlock'=>'changeEnsA'])?>"  style="color:white;">Читать далее</a>
                </div>
            </div>
            <div style="clear:both"></div>
            <hr/>
            <div style="clear:both"></div>
        <?}?>
    </div>

    <?foreach($contractb as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h3><?=$v->title?></h3>
                <?=\app\controllers\SearchController::cutStr($v->content, 800)?>
                <a href="<?=URL::toRoute(['site/change-ensure','activeBlock'=>'changeEnsB'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>


    <?foreach($contractc as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h3><?=$v->title?></h3>
                <?=\app\controllers\SearchController::cutStr($v->content, 800)?>
                <a href="<?=URL::toRoute(['site/change-ensure','activeBlock'=>'changeEnsC'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>



    <?foreach($aboutInf as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h3><?=$v->title?></h3>
                <?=\app\controllers\SearchController::cutStr($v->content, 2000)?>
                <a href="<?=URL::toRoute(['site/about','activeBlock'=>'inf'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>


    <?foreach($aboutRep as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">

                <?=\app\controllers\SearchController::cutStr($v->content, 800)?>
                <a href="<?=URL::toRoute(['site/about','activeBlock'=>'rep'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>



    <?foreach($aboutBank as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h4><?=$v->title?></h4>
                <div class="separator" style="color:black;"></div>
                <h6><?=$v->subtitle?></h6>
                <p>
                    РНН : <br>
                    <?=$v->rnn?>
                <p>...</p>
                </p>
                <a href="<?=URL::toRoute(['site/about','activeBlock'=>'bank'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>


    <?foreach($aboutVacancy as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h3><?=$v->name?></h3>
                <h6><?=$v->name?></h6>
                <p><?=\app\controllers\SearchController::cutStr($v->content, 100)?></p>
                <a href="<?=URL::toRoute(['site/about','activeBlock'=>'job'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>


    <?foreach($aboutManual as $v){?>
        <div class="head-item wow fadeInRight" data-wow-delay="0.1s">
            <h6><?=$v->fullname?></h6>
            <div class="small-sep"></div>
            <p><?=$v->position?>
            </p>
            <div class="item-img">
                <img src="<?=$v->getImage()?>" alt="">
            </div>
            <a href="<?=URL::toRoute(['site/about','activeBlock'=>'man'])?>"  style="color:white;">Посмотреть на странице</a>
        </div>
    <?}?>


    <?foreach($aboutLicense as $v){?>
        <div class="licence-item wow slideInUp" data-wow-delay="0.1s">
            <div class="inner-text">
                <p><b><?=$v['title']?> </b></p>
            </div>
            <a href="<?=$v->getImage();?> " class="sb"><img src="<?=$v->getImage();?>" alt=""></a>
            <a href="<?=URL::toRoute(['site/about','activeBlock'=>'lic'])?>"  style="color:white;">Посмотреть на странице</a>
        </div>
    <?}?>


<!---->
<!--    --><?//foreach($productCorp as $v){?>
<!--        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">-->
<!--            <div class="main__section1__block2">-->
<!--                <h3>--><?//=$v->title?><!--</h3>-->
<!--                --><?//=\app\controllers\SearchController::cutStr($v->content, 400)?>
<!--                <a href="--><?//=URL::toRoute(['site/product-corp'])?><!--"  style="color:white;">Читать далее</a>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div style="clear:both"></div>-->
<!--        <hr/>-->
<!--        <div style="clear:both"></div>-->
<!--    --><?//}?>


<!--    --><?//foreach($productFamily as $v){?>
<!--        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">-->
<!--            <div class="main__section1__block2">-->
<!--                <h3>--><?//=$v->title?><!--</h3>-->
<!--                --><?//=\app\controllers\SearchController::cutStr($v->content, 400)?>
<!--                <a href="--><?//=URL::toRoute(['site/product-family'])?><!--"  style="color:white;">Читать далее</a>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div style="clear:both"></div>-->
<!--        <hr/>-->
<!--        <div style="clear:both"></div>-->
<!--    --><?//}?>


<!--    --><?//foreach($productCapital as $v){?>
<!--        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">-->
<!--            <div class="main__section1__block2">-->
<!--                <h3>--><?//=$v->title?><!--</h3>-->
<!--                --><?//=\app\controllers\SearchController::cutStr($v->content, 400)?>
<!--                <a href="--><?//=URL::toRoute(['site/product-capital'])?><!--"  style="color:white;">Читать далее</a>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div style="clear:both"></div>-->
<!--        <hr/>-->
<!--        <div style="clear:both"></div>-->
<!--    --><?//}?>


    <?foreach($productEnsure as $v){?>
        <div data-aos="fade-up" data-aos-duration="500" class="aos-init aos-animate div">
            <div class="main__section1__block2">
                <h3><?=$v->title?></h3>
                <?=\app\controllers\SearchController::cutStr($v->content, 400)?>
                <a href="<?=URL::toRoute(['site/product-ensure'])?>"  style="color:white;">Читать далее</a>
            </div>
        </div>
        <div style="clear:both"></div>
        <hr/>
        <div style="clear:both"></div>
    <?}?>


    <?foreach($supportFao as $v){?>
        <div class="qa-block wow fadeInLeft" data-wow-delay="<?=$delay;?>s">
            <div class="qa-section" href="<?="#collapseExample".$v->id;?>" data-toggle="collapse">
                <?=$v->title?>
                <img src="/public/images/plus-ico.png" alt="">
            </div>
            <div class="collapse" id="<?="collapseExample".$v->id;?>">
                <div class="answer">
                    <p> <?=\app\controllers\SearchController::cutStr($v->content, 100)?></p>
                    <a href="<?=URL::toRoute(['site/support','activeBlock'=>'fao'])?>"  style="color:white;">Читать далее</a>
                </div>
            </div>
        </div>
    <?}?>


    <?foreach($supportGlossary as $v){?>
        <div class="qa-block wow fadeInLeft" data-wow-delay="<?=$delay;?>s">
            <div class="qa-section" href="<?="#collapseExample".$v->id;?>" data-toggle="collapse">
                <?=$v->title?>
                <img src="/public/images/plus-ico.png" alt="">
            </div>
            <div class="collapse" id="<?="collapseExample".$v->id;?>">
                <div class="answer">
                    <p> <?=\app\controllers\SearchController::cutStr($v->content, 300)?></p>
                    <a href="<?=URL::toRoute(['site/support','activeBlock'=>'glos'])?>"  style="color:white;">Читать далее</a>
                </div>
            </div>
        </div>
    <?}?>


    <?php foreach ($supportDoc as $document): ?>
        <div class="qa-block">
            <div class="qa-section wow slideInLeft" href="#collapse<?=$document->id;?>" data-toggle="collapse">
                <?=$document->title?>
                <img src="/public/images/plus-ico.png" alt="">
            </div>
            <div class="collapse" id="collapse<?=$document->id;?>">
                <div class="answer-split">
                    <?php $types = $document->documentItems ?>
                    <?php foreach ($types as $type):?>
                        <p><?=$type->name?><a href="<?=$type->getFile()?>" target = "_blank">Скачать PDF</a></p>
                        <a href="<?=URL::toRoute(['site/support','activeBlock'=>'doc'])?>"  style="color:white;">Посмотреть все</a>
                        <?php break;?>
                    <?php endforeach;?>

                </div>
            </div>
        </div>
    <?php endforeach;?>

    <?php foreach ($ensA as $v): ?>
        <div class="ensure-item">
            <img src="<?=$v->getImage()?>" class="wow rotateInUpLeft" alt="">
            <div class="bank-text text-center wow fadeInUp">
                <?=$v->name?>
            </div>
            <a href="<?=URL::toRoute(['site/ensure'])?>" style="color:white;">Посмотреть все</a>
        </div>
    <?php endforeach;?>


    <?php foreach ($ensB as $v): ?>
        <div class="ensure-item">
            <img src="<?=$v->getImage()?>" class="wow rotateInUpLeft" alt="">
            <div class="bank-text text-center wow fadeInUp">
                <?=$v->name?>
            </div>
            <a href="<?=URL::toRoute(['site/ensure'])?>" style="color:white;">Посмотреть все</a>
        </div>
    <?php endforeach;?>

    <?}?>
</div>

</div>
</div>

