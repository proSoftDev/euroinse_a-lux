<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;


if($activeBlock == "inf"){
    $inf = "active";$man="";$lic="";$rep="";$bank="";$job="";
}elseif ($activeBlock == "man"){
    $inf = "";$man="active";$lic="";$rep="";$bank="";$job="";
}elseif ($activeBlock == "lic"){
    $inf = "";$man="";$lic="active";$rep="";$bank="";$job="";
}elseif ($activeBlock == "rep"){
    $inf = "";$man="";$lic="";$rep="active";$bank="";$job="";
}elseif ($activeBlock == "bank"){
    $inf = "";$man="";$lic="";$rep="";$bank="active";$job="";
}elseif ($activeBlock == "job") {
    $inf = "";$man = "";$lic = "";$rep = "";$bank = "";$job = "active";
}

$test =  $_SERVER['REQUEST_URI'];

if(substr($test, count($test)-11,10) == "per-page=6") {$inf = "";$man="";$lic="";$rep="active";$bank="";$job="";}
else if(substr($test, count($test)-11,10) == "per-page=3"){ $inf = "";$man = "";$lic = "";$rep = "";$bank = "";$job = "active";}


if($reportInf["pagination"]->page != 0){$repInf ="active";$repMan = "";$repLic = "";$repText = "";}
elseif($reportManual["pagination"]->page != 0){$repInf ="";$repMan = "active";$repLic = "";$repText = "";}
elseif($reportLicense["pagination"]->page != 0){$repInf ="";$repMan = "";$repLic = "active";$repText = "";}
elseif($reportText["pagination"]->page != 0){$repInf ="";$repMan = "";$repLic = "";$repText = "active";}
else{
    $repInf ="active";$repMan = "";$repLic = "";$repText="";
}
?>
<div class="about-us-content">
    <div class="tabs wow fadeInRight">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link <?=$inf?>" data-toggle="tab" href="#all-info"><?=$about[0]['title']?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$man?>" data-toggle="tab" href="#head"><?=$about[1]['title']?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$lic?>" data-toggle="tab" href="#licence"><?=$about[2]['title']?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$rep?>" data-toggle="tab" href="#report"><?=$about[3]['title']?></a>

            </li>
            <li class="nav-item">
                <a class="nav-link <?=$bank?>" data-toggle="tab" href="#bank"> <?=$about[4]['title']?> </a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$job?>" data-toggle="tab" href="#vacancy"><?=$about[5]['title']?></a>
            </li>
        </ul>
    </div>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane  <?=$inf?>" id="all-info">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$about[0]['subtitle']?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$information[0]['content']?>
                        <div class="download-links space-top">
                            <?php foreach ($docInf as $doc):?>
                                <a href="<?=$doc->getFile()?>" target="_blank"><p><?=$doc->text?></p></a> <br>
                            <?php endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane  <?=$man?>" id="head">
            <div class="head">
                <?php foreach ($manual as $v):?>
                    <div class="inner-title">
                        <p><?=$v->text?></p>
                        <div class="separator">
                        </div>
                    </div>
                    <div class="head-items">
                    <?php $man= $v->manualItems;?>
                    <?php foreach ($man as $employee):?>

                        <div class="head-item wow fadeInRight" data-wow-delay="0.1s">
                            <h6><?=$employee->fullname?></h6>
                            <div class="small-sep"></div>
                            <p><?=$employee->position?>
                            </p>
                            <div class="item-img">
                                <img src="<?=$employee->getImage()?>" alt="">
                                <p><?=$employee->experience?></p>
                            </div>
                        </div>
                    <?php endforeach;?>
                    </div>
                <?php endforeach;?>

            </div>
        </div>
        <div class="tab-pane  <?=$lic?>" id="licence">
            <div class="licence">
                <div class="inner-title wow fadeInLeft">
                    <h4><?=$about[2]['subtitle']?></h4>
                </div>
                <div class="separator"></div>
                <div class="licence-block">
                    <?php foreach ($licenses as $license):?>
                    <div class="licence-item wow slideInUp" data-wow-delay="0.1s">
                        <div class="inner-text">
                            <p><b><?=$license['title']?> </b></p>
                        </div>
                        <a href="<?=$license->getImage();?> " class="sb"><img src="<?=$license->getImage();?>" alt=""></a>
                        <div class="licence-download">
                            <a href="<?=$license->getFile();?>" target="_blank"><p><?=$license->text?></p></a>
                        </div>
                    </div>
                    <? endforeach;?>

                </div>
            </div>
        </div>
        <div class="tab-pane  <?=$bank?>" id="bank">
            <div class="licence">

                <?php foreach ($bankDetails as $property):?>
                    <div class="inner-title wow fadeInUp">
                        <h4><?=$property->title?></h4>
                    </div>
                    <div class="separator"></div>

                        <h6><?=$property->subtitle?></h6>
                        <p>
                            <?php if(Yii::$app->session["lang"] == ""):?>РНН : <br><?php endif;?>
                            <?php if(Yii::$app->session["lang"] == "_kz"):?>СТТН : <br><?php endif;?>
                            <?=$property->rnn?> <br>
                            <?php if(Yii::$app->session["lang"] == ""):?>БИН : <br><?php endif;?>
                            <?php if(Yii::$app->session["lang"] == "_kz"):?>БСН : <br><?php endif;?>
                            <?=$property->bin?> <br>
                            <?php if(Yii::$app->session["lang"] == ""):?> ИИК : <br><?php endif;?>
                            <?php if(Yii::$app->session["lang"] == "_kz"):?> ЖСК : <br><?php endif;?>
                            <?=$property->iik?> <br>
                            БАНК: <br>
                            <?=$property->bank?> <br>
                        </p>

                <? endforeach;?>
            </div>
        </div>
        <div class="tab-pane  <?=$job?>" id="vacancy">
            <div class="licence">
                <div class="inner-title wow slideInLeft">
                    <h4><?=$about[5]['subtitle']?></h4>
                </div>
                <div class="separator"></div>
                <div class="vacancy-block">
                    <?php if(count($vacancies['data']) == 0):?>
                    <div class="no-vacancy wow slideInRight">
                        <div class="bank-text">
                            <?if(Yii::$app->session["lang"] == "_kz"):?>
                                <p>Қазіргі уақытта Компанияда бос жұмыс орындары жоқ, <br>бірақ резюмеңізді қарап шығуға қуаныштымыз.</p>
                            <?php endif;?>
                            <?php if(Yii::$app->session["lang"] == ""):?>
                                <p>В данный момент в Компании нет свободных вакансий, <br>но мы будем рады рассмотреть Ваше резюме.</p>
                            <?php endif;?>
                            <a href="mailto:<?=Yii::$app->view->params['contact'][0]['email']?>"><?=Yii::$app->view->params['contact'][0]['email']?></a>
                        </div>
                    </div>
                    <?php endif;?>
                    <?php foreach ($vacancies['data'] as $vacancy):?>
                    <div class="vacancy-item">
                        <div class="bank-text wow slideInRight">
                            <h6><?=$vacancy->name?></h6>
                            <p><?=$vacancy->content?></p>
                            <p><?=$vacancy->price?> тг / За месяц</p>
                            <ul>
                                <li><img src="/public/images/footer-location.png" alt=""> <?=$vacancy->address?> 	</li>
                                <li><?=$vacancy->getDate()?></li>
                                <li><?=$vacancy->employment?></li>
                            </ul>
                        </div>
                    </div>
                    <? endforeach;?>
                </div>
            </div>
            <?= $this->render('/partials/pagination', [
                'pagination'=>$vacancies['pagination'],
            ]);?>
        </div>
        <div class="tab-pane  <?=$rep?>" id="report">
            <div class="licence">
                <div class="inner-title wow slideInLeft">
                    <h4><?=$about[3]['subtitle']?></h4>
                </div>
                <div class="separator"></div>
                <ul class="nav nav-tabs ">
                    <li class="nav-item">
                        <a class="nav-link <?=$repInf?>" data-toggle="tab" href="#<?=$reports[0]->id?>"><?=$reports[0]->title?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=$repMan?>" data-toggle="tab" href="#<?=$reports[1]->id?>"><?=$reports[1]->title?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=$repLic?>" data-toggle="tab" href="#<?=$reports[2]->id?>"><?=$reports[2]->title?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?=$repText?>" data-toggle="tab" href="#<?=$reports[3]->id?>"><?=$reports[3]->title?></a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane <?=$repInf?>" id="1">
                        <div class="vacancy-block">
                            <?php foreach($reportInf['data'] as $report):?>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a  target="_blank"><p><?=$report->content?></p></a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?= $this->render('/partials/pagination', [
                            'pagination'=>$reportInf['pagination'],
                        ]);?>
                    </div>

                    <div class="tab-pane <?=$repMan?>" id="2">
                        <div class="vacancy-block">
                            <?php foreach($reportManual['data'] as $report):?>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href="<?=$report->getFile()?>" target="_blank"><p><?=$report->content?></p></a>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?= $this->render('/partials/pagination', [
                            'pagination'=>$reportManual['pagination'],
                        ]);?>
                    </div>
                    <div class="tab-pane <?=$repLic?>" id="3">
                        <div class="vacancy-block">
                            <div class="vacancy-block">
                                <?php foreach($reportLicense['data'] as $report):?>
                                    <div class="order-item">
                                        <div class="bank-text wow slideInRight">
                                            <a href="<?=$report->getFile()?>" target="_blank"><p><?=$report->content?></p></a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?= $this->render('/partials/pagination', [
                                'pagination'=>$reportLicense['pagination'],
                            ]);?>
                        </div>
                    </div>


                    <div class="tab-pane <?=$repText?>" id="4">
                        <div class="vacancy-block">
                            <div class="vacancy-block">
                                <?php foreach($reportText['data'] as $report):?>
                                    <div class="order-item">
                                        <div class="bank-text wow slideInRight">
                                            <a href="<?=$report->getFile()?>" target="_blank"><p><?=$report->content?></p></a>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?= $this->render('/partials/pagination', [
                                'pagination'=>$reportText['pagination'],
                            ]);?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Оплата ошибка -->


</div>
</div>
</div>