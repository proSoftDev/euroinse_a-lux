<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Change-ensure';
$this->params['breadcrumbs'][] = $this->title;


if($activeBlock == "changeEnsA"){
    $changeEnsA = "active";$changeEnsB="";$changeEnsC="";
}elseif ($activeBlock == "changeEnsB"){
    $changeEnsA = "";$changeEnsB="active";$changeEnsC="";
}elseif ($activeBlock == "changeEnsC"){
    $changeEnsA = "";$changeEnsB="";$changeEnsC="active";
}
?>

<div class="about-us-content">

    <div class="tabs">
        <ul class="nav nav-tabs ">
            <li class="nav-item">
                <a class="nav-link <?= $changeEnsA?>" data-toggle="tab" href="#2"><?= $contracts["title"][0]['title']?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $changeEnsB?>" data-toggle="tab" href="#3"><?= $contracts["title"][1]['title']?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $changeEnsC?>" data-toggle="tab" href="#4"><?= $contracts["title"][2]['title']?></a>
            </li>
        </ul>
    </div>
    <!-- Tab panes -->

    <div class="tab-content">
        <div class="tab-pane <?= $changeEnsA?>" id="2">

            <div class="tabs">
                <ul class="nav nav-tabs ">
                    <?php $active = 'active';?>
                    <?php foreach ($contracts["contracta"] as $contract):?>
                        <li class="nav-item">
                            <a class="nav-link <?=$active?>" data-toggle="tab" href="#a<?=$contract['id']?>"><?=$contract['title']?></a>
                        </li>
                        <?php $active ="";?>
                    <?php endforeach;?>

                </ul>
            </div>


            <div class="tab-content">
                <?php $active = 'active';?>
                <?php foreach ($contracts["contracta"] as $contract):?>
                    <div class="tab-pane <?=$active?>"  id="a<?=$contract['id']?>">
                        <div class="licence">
                            <div class="product-ensure-text">
                                <?=$contract['content']?>
                            </div>
                        </div>
                    </div>
                    <?php $active ="";?>
                <?php endforeach;?>
            </div>
        </div>
        <div class="tab-pane <?= $changeEnsB?>" id="3">
            <div class="licence">
                <div class="inner-title wow fadeInLeft">
                    <h4><?= $contracts["contractb"][0]["title"]?></h4>
                </div>
                <div class="separator"></div>
                <div class="product-ensure-text wow fadeInUp space-top">
                   <?= $contracts["contractb"][0]["content"]?>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $changeEnsC?>" id="4" >
            <div class="licence">
                <div class="inner-title wow fadeInLeft">
                    <h4><?=$contracts["contractc"][0]["title"]?></h4>
                </div>
                <div class="separator"></div>
                <div class="product-ensure-text wow fadeInUp space-top">
                    <?=$contracts["contractc"][0]["content"]?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Оплата ошибка -->

</div>
</div>
</div>
