<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Ensure';
$this->params['breadcrumbs'][] = $this->title;
?>



<div class="ensure-content">
    <div class="european-pay-title wow fadeInDown">
        <h4><?=$ensTitle[0]->title?></h4>
    </div>
    <div class="bank-text space-top wow fadeInUp">
        <?=$ensTitle[0]->subtitle?>
    </div>
    <div class="inner-title space-top wow fadeInLeft">
        <h4><?=$ensSub[0]->subtitle?></h4>
        <div class="separator"></div>
    </div>
    <div class="ensure-block">
        <div class="ensure-item one">
            <img src="<?=$ensA[0]->getImage()?>" class="wow rotateInUpLeft" alt="">
            <div class="bank-text text-center wow fadeInUp">
               <?=$ensA[0]->name?>
            </div>
            <div class="modal-window1">
                <div class="btn-call-loc">
                    <?=$ensA[0]->inname?>
                </div>
            </div>
        </div>
        <div class="ensure-item two">
            <img src="<?=$ensA[1]->getImage()?>" class="wow rotateInUpLeft" data-wow-delay="0.1s" alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.1s" >
                <?=$ensA[1]->name?>
            </div>
            <div class="modal-window2">
                <div class="modal-link list-text">
                    <?=$ensA[1]->inname?>
                    <ol>
                        <?foreach ($ensAList as $list):?>
                            <?php $doclinks = ""?>
                            <?php foreach ($ensAListDoc as $doc):?>
                                <?php if($list->id == $doc->insurancealist_id):?>
                                    <?php $doclinks = $doclinks."window.open('".$doc->getFile()."', '_blank');"?>
                                <?php endif;?>
                            <?php endforeach;?>
                            <li><a onclick="<?=$doclinks?>"><?=$list->name?></a></li>
                            <?php $inlist = $list->items;?>
                            <ul>
                                <?php foreach ($inlist as $in):?>
                                    <?php $doclinks = ""?>
                                    <?php foreach ($ensATypeDoc as $doc):?>
                                        <?php if($in->id == $doc->insuranceatype_id):?>
                                            <?php $doclinks = $doclinks."window.open('".$doc->getFile()."', '_blank');"?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                    <li><a onclick="<?=$doclinks?>"><?=$in->name?></a></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endforeach;?>
                    </ol>
                </div>

            </div>
        </div>
        <div class="ensure-item seven">
            <img src="<?=$ensA[2]->getImage()?>" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                <?=$ensA[2]->name?>
            </div>
            <div class="modal-window7">
                <div class="modal-text pad-top">
                    <?=$ensA[2]->inname?>
                </div>
            </div>
        </div>
        <div class="ensure-item three">
            <img src="<?=$ensA[3]->getImage()?>" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                <?=$ensA[3]->name?>
            </div>
            <div class="modal-window3">
                <div class="modal-text pad-top">
                    <?=$ensA[3]->inname?>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-title space-top wow fadeInLeft">
        <h4><?=$ensSub[1]->subtitle?></h4>
        <div class="separator"></div>
    </div>
    <div class="ensure-block">
        <div class="ensure-item eight">
            <img src=" <?=$ensB[0]->getImage()?>" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                <?=$ensB[0]->name?>
            </div>
            <div class="modal-window8">
                <div class="modal-text pad-top">
                    <?=$ensB[0]->inname?>
                </div>
            </div>
        </div>
        <div class="ensure-item four">
            <img src=" <?=$ensB[1]->getImage()?>" class="wow rotateInUpRight" data-wow-delay="0.4s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.4s">
                <?=$ensB[1]->name?>
            </div>
            <div class="modal-window4">
                <div class="modal-text pad-top2">
                    <?=$ensB[1]->inname?>
                </div>
            </div>
        </div>
        <div class="ensure-item five">
            <img src=" <?=$ensB[2]->getImage()?>" class="wow rotateInUpRight" data-wow-delay="0.5s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.5s">
                <?=$ensB[2]->name?>
            </div>
            <div class="modal-window5">
                <div class="modal-text pad-top2">
                    <?=$ensB[2]->inname?>
                </div>
            </div>
        </div>
        <div class="ensure-item six">
            <img src=" <?=$ensB[3]->getImage()?>" class="wow rotateInUpRight" data-wow-delay="0.6s"  alt="">
            <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.6s">
                <?=$ensB[3]->name?>
            </div>
            <div class="modal-window6">
                <div class="modal-text pad-top2">
                    <?=$ensB[3]->inname?>
                </div>
            </div>
        </div>
    </div>
    <div class="inner-title space-top wow slideInLeft">
        <h4><?=$ensSub[2]->subtitle?></h4>
        <div class="separator"></div>
    </div>
    <div class="ensure-block">
        <a href="" class="btn-ensure big1 wow fadeInLeft"><?=$ensC[0]->title?></a>
        <a href=""  class="btn-ensure big2 wow fadeInRight"><?=$ensC[1]->title?></a>
        <div class="modal-big1">
            <div class="modal-big-title">

                <?php foreach ($ensCa as $content):?>

                <h5><?=$content->name?></h5>
                <div class="modal-separator"></div>
                <div class="close-btn close1">
                    <a style="cursor: pointer">&#10005</a>
                </div>
                <div class="row">
                    <? $m=0;?>
                    <? $documents = $content->documentItems;?>
                    <?php for($i=0;$i<(count($documents)/3);$i++):?>
                    <div class="col-sm-4">
                        <div class="modal-big-text">
                            <?php for($k=0;$k<count($documents);$k++):?>
                                <?php if($m<($i+1)*3):?>
                                    <a href="<?="/uploads/pdf/".$documents[$m]->file?>" target = "_blank"><p><?=$documents[$m]->name?></p></a>
                                    <?php $m++;?>
                                <?php endif;?>

                            <?php endfor;?>
                        </div>
                    </div>
                    <? endfor;?>
                </div>
                <? endforeach;?>
            </div>
        </div>
        <div class="modal-big2">
            <div class="modal-big-title">
                <?php foreach ($ensCb as $content):?>

                    <h5><?=$content->name?></h5>
                    <div class="modal-separator"></div>
                    <div class="close-btn close2">
                        <a style="cursor: pointer">&#10005</a>
                    </div>
                    <div class="row">
                        <? $m=0;?>
                        <? $documents = $content->documentItems;?>
                        <?php for($i=0;$i<(count($documents)/3);$i++):?>
                            <div class="col-sm-4">
                                <div class="modal-big-text">
                                    <?php for($k=0;$k<count($documents);$k++):?>
                                        <?php if($m<($i+1)*3):?>
                                            <a href="<?="/uploads/pdf/".$documents[$m]->file?>" target = "_blank"><p><?=$documents[$m]->name?></p></a>
                                            <?php $m++;?>
                                        <?php endif;?>

                                    <?php endfor;?>
                                </div>
                            </div>
                        <? endfor;?>
                    </div>
                <? endforeach;?>
            </div>
        </div>
    </div>
</div>

<!-- End Оплата ошибка -->



</div>
</div>
</div>

