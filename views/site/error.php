<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="european-content" >
    <div class="site-error" style="padding-bottom: 200px;">

        <h1>Не найден (# 404)</h1>

        <div class="alert alert-danger">
            Страница не найдена.
        </div>

        <p>
            Вышеуказанная ошибка произошла, когда веб-сервер обрабатывал ваш запрос.
        </p>
        <p>
            Пожалуйста, свяжитесь с нами, если считаете, что это ошибка сервера. Спасибо.
        </p>

    </div>
<div >
