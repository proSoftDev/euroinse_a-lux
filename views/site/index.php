<?php

/* @var $this yii\web\View */

$this->title = 'European main';
$m = 0;
use yii\helpers\Url; ?>
<div class="european-content">
    <!-- 1 СТРОКА -->
    <div class="european-item-wrap">
        <?php if($this->params['menu'][4]["status"] == 1):?>
            <div class="european-insurance-case european-item" style='
                  background: linear-gradient(rgba(0, 0, 0, 0.4) ,
                  rgba(0, 0, 0, 0.4)),
                  rgba(0, 0, 0, 0.4)
                  url(<?=$menus[4]->getImage();?>) no-repeat;
                  -webkit-background-size: cover;
                  background-size: cover;
                  text-align: center;
                    '>
                <a href="<?= Url::toRoute(['site/ensure'])?>"><?=$menus[4]["title"]?></a>
            </div>
        <?php endif;?>
        <?php if($this->params['menu'][6]["status"] == 1):?>
            <div class="european-change-insurance european-item" style='background-image: url("<?=$menus[6]->getImage();?>")'>
                <a href="<?= Url::toRoute(['site/change-ensure','activeBlock'=>'changeEnsA'])?>"><?=$menus[6]["title"]?></a>
            </div>
        <?php endif;?>
        <?php if($this->params['menu'][2]["status"] == 1):?>
            <div class="european-products european-item" style='background-image: url("<?=$menus[2]->getImage();?>")'>
                <a href="<?= Url::toRoute(['/product'])?>"><?=$menus[2]["title"]?></a>
            </div>
        <?php endif;?>
    </div>


    <!-- END 1 СТРОКА -->


    <!-- 2 СТРОКА -->
    <div class="european-item-wrap">
        <?php if($this->params['menu'][5]["status"] == 1):?>
            <div class="european-pay european-item" style='background-image: url("<?=$menus[5]->getImage();?>")'>
                <a href="<?= Url::toRoute(['site/pay'])?>"><?=$menus[5]["title"]?></a>
            </div>
        <?php endif;?>
        <?php if($this->params['menu'][3]["status"] == 1):?>
            <div class="european-client-support european-item" style='background-image: url("<?=$menus[3]->getImage();?>")'>
                <a href="<?= Url::toRoute(['site/support','activeBlock'=>'cont'])?>"><?=$menus[3]["title"]?></a>
            </div>
        <?php endif;?>
        <?php if($this->params['menu'][7]["status"] == 1):?>
            <div class="european-contacts european-item" style='background-image: url("<?=$menus[7]->getImage();?>")'>
                <a href="<?= Url::toRoute(['site/support','activeBlock'=>'cont'])?>"><?=$menus[7]["title"]?></a>
            </div>
        <?php endif;?>
    </div>
    <!-- END 2 СТРОКА -->

</div>

</div>