<?php

use yii\helpers\Html;

if($activeBlock == "inf"){
    $inf = "active"; $man = ""; $lic = ""; $rep = ""; $bank = ""; $con = ""; $job = "";
} elseif ($activeBlock == "man") {
    $inf = "";$man = "active";$lic = "";$rep = "";$bank = "";$con = "";$job = "";
} elseif ($activeBlock == "lic") {
    $inf = "";$man = "";$lic = "active";$rep = "";$bank = "";$con = "";$job = "";
} elseif ($activeBlock == "rep") {
    $inf = "";$man = "";$lic = "";$rep = "active";$bank = "";$con = "";$job = "";
} elseif ($activeBlock == "bank") {
    $inf = "";$man = "";$lic = "";$rep = "";$bank = "active";$con = "";$job = "";
} elseif ($activeBlock == "con") {
    $inf = "";$man = "";$lic = "";$rep = "";$bank = "";$con = "active";$job = "";
} elseif ($activeBlock == "job") {
    $inf = "";$man = ""; $lic = "";$rep = "";$bank = "";$con = "";$job = "active";
}

$test =  $_SERVER['REQUEST_URI'];

if(substr($test, count($test)-11,10) == "per-page=6") {$inf = "";$man="";$lic="";$rep="active";$bank="";$job="";}
else if(substr($test, count($test)-11,10) == "per-page=3"){ $inf = "";$man = "";$lic = "";$rep = "";$bank = "";$job = "active";}


?>

<div class="about-us-content">
    <div class="tabs wow fadeInRight">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link <?= $inf ?> active show" data-toggle="tab" href="#all-info"><?=$payInfo[0]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$man?>" data-toggle="tab" href="#guarantees"><?=$payInfo[1]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?=$lic?>" data-toggle="tab" href="#security"><?=$payInfo[2]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $rep ?>" data-toggle="tab" href="#return"><?=$payInfo[3]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $bank ?>" data-toggle="tab" href="#failurecases"><?=$payInfo[4]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $con ?>" data-toggle="tab" href="#confidentiality"><?=$payInfo[5]->title;?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $job ?>" data-toggle="tab" href="#legalentity"><?=$payInfo[6]->title;?></a>
            </li>
        </ul>
    </div>
    <div class="tab-content">
        <div class="tab-pane <?=$inf?> active show" id="all-info">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[0]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[0]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $man ?>" id="guarantees">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[1]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[1]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $lic ?>" id="security">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[2]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[2]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $rep ?>" id="return">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[3]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[3]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $bank ?>" id="failurecases">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[4]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[4]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $con ?>" id="confidentiality">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[5]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[5]->text;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane <?= $job ?>" id="legalentity">
            <div class="all-info">
                <div class="inner-title">
                    <h4 class="wow fadeInLeft"><?=$payInfo[6]->subtitle;?></h4>
                    <div class="separator"></div>
                    <div class="inner-text wow fadeInUp">
                        <?=$payInfo[6]->text;?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>
</div>
</div>