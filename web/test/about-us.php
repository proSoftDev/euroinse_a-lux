<?php require_once('menu.php');?>

    <div class="about-us-content">
        <div class="tabs wow fadeInRight">
            <ul class="nav nav-tabs ">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#all-info">Общая информация </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head">Руководство</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#licence">Лицензия</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#report">Отчетность</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#bank">Банковские реквизиты </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#vacancy">Вакансии</a>
                </li>
            </ul>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active " id="all-info">
                <div class="all-info">
                    <div class="inner-title">
                        <h4 class="wow fadeInLeft">АО «ЕВРОПЕЙСКАЯ СТРАХОВАЯ КОМПАНИЯ»</h4>
                        <div class="separator"></div>
                        <div class="inner-text wow fadeInUp">
                            <p>Компания основана в 2006 году, является первой и единственной иностранной компанией со 100% участием иностранного капитала (ранее работала под брендами «Generali Life» и «PPF Insurance»).
                            </p>
                            <p>По корпоративному направлению бизнеса Компания в Казахстане является официальным представителем Generali Employee Benefits Network (GEB). Клиентами нашей организации являются крупнейшие международные компании, которые доверяют страховую защиту своих сотрудников команде профессионалов. Опыт и гибкие  программы страхования  в корпоративном сегменте, позволяют предлагать своим клиентам надежную страховую защиту, ориентированным на удовлетворение всех потребностей компании и ее сотрудников.</p>

                            <p>Опыт и международные программы GEB в корпоративном сегменте, позволяют предлагать своим клиентам надежную страховую защиту по программам, ориентированным на удовлетворение всех потребностей компании и ее сотрудников.</p>
                            <b>Краткая информация о АО «Европейской Страховой Компании» (на конец 2017 года):</b>
                            <ul>
                                <li>оплаченный уставной капитал - 1 000 000 000 тенге</li>
                                <li>объем страховых премий - 17 363 691 тенге</li>
                                <li>суммарный объем активов - 13 289 902 тенге</li>
                            </ul>
                            <div class="download-links space-top">
                                <a href=""><p>Справка о всех регистрационных действиях юридического лица</p></a> <br>
                                <a href=""><p>Справка о государственной перерегистрации юридического лица на РУС</p></a> <br>
                                <a href=""><p>Справка о государственной перерегистрации юридического лица на КАЗ</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="head">
                <div class="head">
                    <div class="inner-title">
                        <h4 class="wow fadeInLeft">PУКОВОДСТВO</h4>
                        <p>Единственный Акционер - Рихард Бенишек с <b>2014</b> года, Чешская Республика</p>
                        <div class="separator">
                        </div>
                    </div>
                    <div class="head-items">
                        <div class="head-item wow fadeInRight" data-wow-delay="0.1s">
                            <h6>Надиров Дмитрий Надирович</h6>
                            <div class="small-sep"></div>
                            <p><b>Председатель Правления</b> <br>
                                с <b>2011</b> г.
                            </p>
                            <div class="item-img">
                                <img src="images/head1.jpg" alt="">
                                <p>Опыт работы в страховании с <b>1996</b> г.</p>
                            </div>
                        </div>
                        <div class="head-item wow fadeInRight" data-wow-delay="0.2s">
                            <h6>Байгарин Надир Рустемович</h6>
                            <div class="small-sep"></div>
                            <p><b>Исполнительный Директор</b> <br>
                                Член Правления с <b>2007</b> г.
                            </p>
                            <div class="item-img">
                                <img src="images/head1.png" alt="">
                                <p>Опыт работы в страховании с <b>2007</b> г.</p>
                            </div>
                        </div>
                        <div class="head-item wow fadeInRight" data-wow-delay="0.3s">
                            <h6>Усипбекова Айгуль Асановна</h6>
                            <div class="small-sep"></div>
                            <p><b>Главный Бухгалтер</b> <br>
                                с <b>2011</b> г.
                            </p>
                            <div class="item-img">
                                <img src="images/head3.png" alt="">
                                <p>Опыт работы в страховании с <b>2009</b> г.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane " id="licence">
                <div class="licence">
                    <div class="inner-title wow fadeInLeft">
                        <h4>ЛИЦЕНЗИИ</h4>
                    </div>
                    <div class="separator"></div>
                    <div class="licence-block">
                        <div class="licence-item wow slideInUp" data-wow-delay="0.1s">
                            <div class="inner-text">
                                <p><b>Лицензия на русском языке </b></p>
                            </div>
                            <a href="images/lic-ru.jpg" class="sb"><img src="images/lic-ru.jpg" alt=""></a>
							<div class="licence-download">
								<a href="" ><p>Скачать лицензию </br>на русском языке в PDF</p></a>
							</div>
                        </div>
                        <div class="licence-item wow slideInUp" data-wow-delay="0.2s">
                            <div class="inner-text">
                                <p><b>Лицензия на казахском языке  </b></p>
                            </div>
                            <a href="images/lic-kz.jpg" class="sb"><img src="images/lic-kz.jpg" alt=""></a>
							<div class="licence-download">
								<a href="" ><p>Скачать лицензию </br> на казахском языке в PDF</p></a>
							</div>							
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="bank">
                <div class="licence">
                    <div class="inner-title wow fadeInUp">
                        <h4>Реквизиты для оплаты по договорам накопительного страхования</h4>
                    </div>
                    <div class="separator"></div>
                    <div class="bank-text wow fadeInUp">
                        <h6>АКЦИОНЕРНОЕ ОБЩЕСТВО КОМПАНИЯ ПО СТРАХОВАНИЮ ЖИЗНИ «ЕВРОПЕЙСКАЯ СТРАХОВАЯ КОМПАНИЯ»</h6>
                        <p>
                            РНН : <br>
                            600900583405 <br>
                            БИН : <br>
                            061240008233 КБЕ 15 <br>
                            ИИК : <br>
                            KZ2883201T0250228023 <br>
                            БАНК: <br>
                            АО «СИТИБАНК КАЗАХСТАН» <br>
                        </p>
                    </div>
                    <div class="inner-title wow fadeInUp">
                        <h3>Для других платежей</h3>
                    </div>
                    <div class="separator"></div>
                    <div class="bank-text">
                        <h6>АКЦИОНЕРНОЕ ОБЩЕСТВО КОМПАНИЯ ПО СТРАХОВАНИЮ ЖИЗНИ «ЕВРОПЕЙСКАЯ СТРАХОВАЯ КОМПАНИЯ»</h6>
                        <p>
                            РНН : <br>
                            600900583405 <br>
                            БИН : <br>
                            061240008233 КБЕ 15 <br>
                            ИИК : <br>
                            KZ2883201T0250228023 <br>
                            БАНК: <br>
                            АО «СИТИБАНК КАЗАХСТАН» <br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="vacancy">
                <div class="licence">
                    <div class="inner-title wow slideInLeft">
                        <h4>Вакансии</h4>
                    </div>
                    <div class="separator"></div>
                    <div class="vacancy-block">
                        <div class="no-vacancy wow slideInRight" style="display: none">
                            <div class="bank-text">
                                <p>В данный момент в Компании нет свободных вакансий, <br>но мы будем рады рассмотреть Ваше резюме.</p>
                                <a href="">info@euroins.kz</a>
                            </div>
                        </div>
                        <div class="vacancy-item">
                            <div class="bank-text wow slideInRight">
                                <h6>Название вакансии</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>000 000 тг / За месяц</p>
                                <ul>
                                    <li><img src="images/footer-location.png" alt=""> Алматы, Бостандыкский район 	</li>
                                    <li>9 декабря</li>
                                    <li>Полная занятость</li>
                                </ul>
                            </div>
                        </div>
                        <div class="vacancy-item">
                            <div class="bank-text wow slideInLeft">
                                <h6>Название вакансии</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>000 000 тг / За месяц</p>
                                <ul>
                                    <li><img src="images/footer-location.png" alt=""> Алматы, Бостандыкский район 	</li>
                                    <li>9 декабря</li>
                                    <li>Полная занятость</li>
                                </ul>
                            </div>
                        </div>
                        <div class="vacancy-item">
                            <div class="bank-text wow slideInRight">
                                <h6>Название вакансии</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                <p>000 000 тг / За месяц</p>
                                <ul>
                                    <li><img src="images/footer-location.png" alt=""> Алматы, Бостандыкский район 	</li>
                                    <li>9 декабря</li>
                                    <li>Полная занятость</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <nav aria-label="page navigation example" >
                    <ul class="pagination wow fadeInUp" data-wow-delay="1s" >
                        <li class="page-item"><a class="page-link" href="#">В начало</a></li>
                        <li class="page-item"><a class="page-link" href="#">Назад</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">2</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">3</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">Вперёд</a></li>
                        <li class="page-item"><a class="page-link" href="#">В конец</a></li>
                    </ul>
                </nav>
            </div>
            <div class="tab-pane " id="report">
                <div class="licence">
                    <div class="inner-title wow slideInLeft">
                        <h4>Отчетность</h4>
                    </div>
                    <div class="separator"></div>
                    <ul class="nav nav-tabs ">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#1">Членство в ассоциациях </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#2">Тарифы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#3">Отчетность</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="1">
                            <div class="vacancy-block">
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2011 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2012 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2013 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2014 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 1-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 2-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 3-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="2">
                            <div class="vacancy-block">
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2011 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2012 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2013 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2014 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 1-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 2-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 3-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="3">
                            <div class="vacancy-block">
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2011 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2012 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2013 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Аудированная финансовая отчётность за 2014 год</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 1-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 2-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                                <div class="order-item">
                                    <div class="bank-text wow slideInRight">
                                        <a href=""><p>Неаудированная финансовая отчётность (бухгалтерский баланс и отчет о прибылях и убытках) за 3-й квартал 2013 года</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <nav aria-label="page navigation example" >
                    <ul class="pagination wow fadeInUp" data-wow-delay="1s" >
                        <li class="page-item"><a class="page-link" href="#">В начало</a></li>
                        <li class="page-item"><a class="page-link" href="#">Назад</a></li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">2</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">3</a></li>
                        <li class="page-item hidden-sm"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                        <li class="page-item"><a class="page-link" href="#">Вперёд</a></li>
                        <li class="page-item"><a class="page-link" href="#">В конец</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Оплата ошибка -->



    </div>
    </div>
    </div>

<?php require_once('footer.php');?>