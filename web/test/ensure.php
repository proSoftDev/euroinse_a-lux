<?php require_once('menu.php');?>

    <div class="ensure-content">
        <div class="european-pay-title wow fadeInDown">
            <h4>Страховой случай</h4>
        </div>
        <div class="bank-text space-top wow fadeInUp">
            <p>Для индивидуальной консультации по страховым случая, вам необходимо обратится <br> по номеру телефона
                <a href="">8-727-244-36-80</a> (колл центр), либо по номеру WP
                <a href="">8-701-053-26-42</a></p>
        </div>
        <div class="inner-title space-top wow fadeInLeft">
            <h4>При наступлении страхового случая Вам необходимо:</h4>
            <div class="separator"></div>
        </div>
        <div class="ensure-block">
            <div class="ensure-item one">
                <img src="images/ensure1.png" class="wow rotateInUpLeft" alt="">
                <div class="bank-text text-center wow fadeInUp">
                    <p>Позвонить в страховую <br>
                        компанию <br>
                        <b>+7 (727) 244 36 80</b>
                    </p>
                </div>
                <div class="modal-window1">
                    <div class="btn-call-loc">
                        <a href="" class="btn-call">Позвонить</a>
                    </div>
                </div>
            </div>
            <div class="ensure-item two">
                <img src="images/ensure2.png" class="wow rotateInUpLeft" data-wow-delay="0.1s" alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.1s" >
                    <p>Собрать документы по списку, для предоставления страховой компании</p>
                </div>
                <div class="modal-window2">
                    <div class="modal-link list-text">
                        <p>Выберите нужное:</p>
                        <ol>
                            <li><a href="">Временная нетрудоспособность</a></li>
                            <li><a href="">Телесные повреждения</a></li>
                            <li><a href="">Установление инвалидности</a>
                                <ul>
                                    <li><a href="">заболевание</a></li>
                                    <li><a href="">несчастный случай</a></li>
                                </ul>
                            </li>
                            <li><a href=""><p>Смерть</p></a>
                                <ul>
                                    <li><a href="">заболевание</a></li>
                                    <li><a href="">несчастный случай</a></li>
                                </ul>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="ensure-item seven">
                <img src="images/ensure7.png" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                    <p>Выслать документы почтой</p>
                </div>
                <div class="modal-window7">
                    <div class="modal-text pad-top">
                        <p>Выслать документы <br> на проверку в Ватсапп</p>
                        <div class="call-modal">
                            <div class="modal-ico">
                                <img src="images/modal-ico.png" alt="">
                            </div>
                            <a href="">87010532642</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ensure-item three">
                <img src="images/ensure3.png" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                    <p>Выслать документы почтой</p>
                </div>
                <div class="modal-window3">
                    <div class="modal-text pad-top">
                        <p>Выслать документы <br>на проверку специалисту</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-title space-top wow fadeInLeft">
            <h4>Действия страховой компании:</h4>
            <div class="separator"></div>
        </div>
        <div class="ensure-block">
            <div class="ensure-item eight">
                <img src="images/ensure7.png" class="wow rotateInUpLeft" data-wow-delay="0.2s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.2s" >
                    <p>Проверка документов <br>в Ватсаппе</p>
                </div>
                <div class="modal-window8">
                    <div class="modal-text pad-top">
                        <p>Получить документы <br>
                            через Ватсапп</p>
                    </div>
                </div>
            </div>
            <div class="ensure-item four">
                <img src="images/ensure4.png" class="wow rotateInUpRight" data-wow-delay="0.4s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.4s">
                    <p>Получить полный пакет <br>документов через почту</p>
                </div>
                <div class="modal-window4">
                    <div class="modal-text pad-top2">
                        <p>Получить документы <br>
                            через почту</p>
                    </div>
                </div>
            </div>
            <div class="ensure-item five">
                <img src="images/ensure5.png" class="wow rotateInUpRight" data-wow-delay="0.5s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.5s">
                    <p>Рассмотрение полученных документов</p>
                </div>
                <div class="modal-window5">
                    <div class="modal-text pad-top2">
                        <p>Рассмотрение в течение <br> <b>30 рабочих дней</b></p>
                    </div>
                </div>
            </div>
            <div class="ensure-item six">
                <img src="images/ensure6.png" class="wow rotateInUpRight" data-wow-delay="0.6s"  alt="">
                <div class="bank-text text-center wow fadeInUp" data-wow-delay="0.6s">
                    <p>Принятие решения <br>по страховым случаям</p>
                </div>
                <div class="modal-window6">
                    <div class="modal-text pad-top2">
                        <p>Выплата</p>
                        <p>Отказ</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-title space-top wow slideInLeft">
            <h4>Условия страхования</h4>
            <div class="separator"></div>
        </div>
        <div class="ensure-block">
            <a href="" class="btn-ensure big1 wow fadeInLeft">Правила <br> страхования</a>
            <a href="" class="btn-ensure big2 wow fadeInRight">Примерные условия <br>страхования</a>
            <div class="modal-big1">
                <div class="modal-big-title">
                    <h5>Страхование заемщиков</h5>
                    <div class="modal-separator"></div>
                    <div class="close-btn close1">
                        <a href="">&#10005</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Корпоративное страхование</h5>
                    <div class="modal-separator"></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Защита семьи</h5>
                    <div class="modal-separator"></div>
                    <div class="close-btn close1">
                        <a href="">&#10005</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Накопительное страхование</h5>
                    <div class="modal-separator"></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-big2">
                <div class="modal-big-title">
                    <h5>Правила страхования</h5>
                    <div class="modal-separator"></div>
                    <div class="close-btn close1">
                        <a href="">&#10005</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Правила страхования</h5>
                    <div class="modal-separator"></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Правила страхования</h5>
                    <div class="modal-separator"></div>
                    <div class="close-btn close1">
                        <a href="">&#10005</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                    <h5>Правила страхования</h5>
                    <div class="modal-separator"></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="modal-big-text">
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                                <a href=""><p>Lorem ipsum dolor sit amet, consectetur</p></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- End Оплата ошибка -->



    </div>
    </div>
    </div>

<?php require_once('footer.php');?>