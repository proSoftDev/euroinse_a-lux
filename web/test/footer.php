	<footer class="footer" id="footer">

		<!-- СТРЕЛКА ВВЕРХ -->
		<div class="arrow-up wow rotateInUpLeft">
			<a href="#main">
				<img src="images/arrow-up.png" alt="Вверх">
			</a>
		</div>
		<!-- END СТРЕЛКА ВВЕРХ -->
		
		<div class="footer-content">
			<div class="foooter-about-company footer-item wow fadeInUp">
				<h3>О компании</h3>
				<ul>
					<li><a href="#">Общая информация</a></li>
					<li><a href="#">Руководство</a></li>
					<li><a href="#">Лицензия</a></li>
					<li><a href="#">Отчетность</a></li>
					<li><a href="#">Банковские реквизиты</a></li>
					<li><a href="#">Вакансии</a></li>
				</ul>
			</div>

			<div class="foooter-products footer-item wow fadeInUp" data-wow-delay="0.1s">
				<h3>Продукты</h3>
				<ul>
					<li><a href="#">Страхование заёмщиков</a></li>
					<li><a href="#">Корпоративное страхование</a></li>
					<li><a href="#">Защита семьи</a></li>
					<li><a href="#">Накопительное страхование</a></li>
				</ul>
                <a href=""><h3 class="space-top2">Страховой случай</h3></a>

			</div>

			<div class="foooter-support footer-item wow fadeInUp" data-wow-delay="0.2s">
				<h3>Клиентская Поддержка</h3>
				<ul>
					<li><a href="#">Контакты</a></li>
					<li><a href="#">Вопросы и ответы</a></li>
					<li><a href="#">Словарь страховых терминов</a></li>
					<li><a href="#">Формы документов</a></li>
					<li><a href="#">Поиск</a></li>
				</ul>
                <a href=""><h3>Оплата</h3></a>
			</div>

			<div class="foooter-support footer-item wow fadeInUp" data-wow-delay="0.3s">
				<h3>Контакты</h3>
				<ul>
					<li><span><img src="images/footer-phone.png" alt="Телефон"></span><a href="#">+7 727 244 36 80</a></li>
					<li><span><img src="images/footer-whatsapp.png" alt="Whatsapp"></span><a href="#">+7 701 084 10 28</a></li>
					<li><span><img src="images/footer-message.png" alt="Сообщение"></span><a href="#">info@euroins.kz</a></li>
					<li><span><img src="images/footer-location.png" alt="Локация"></span><a href="#">пр-т Нурсултан Назарбаева 248
                            <br ><span style="margin-left: 30px;">уг.ул. Хаджимукана, БЦ Сарканд</span></a></li>
					<li><span><img src="images/footer-clock.png" alt="График работы"></span><a href="#">График работы: Пн.-Пт. 09:00-18:00</a></li>
					<li><span><img src="images/footer-map.png" alt="Карта сайта"></span><a href="#">Карта сайта</a></li>
				</ul>
			</div>
		</div>

	</footer>

	<div class="copyright wow fadeInLeft">
		<p>© 2018 European Insurance Company   Все права защищены.</p>
	</div>
    
    <div class="dev-alux">
        <a href="">Разработано в<img src="images/alux-dark.png" class="img-fluid" alt=""></a>
    </div>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
	<script src="js/wow.min.js"></script>
	<script src="js/jquery.mask.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/main.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>
    <script type="text/javascript" src="js/smoothbox.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
