<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>European main</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link rel="stylesheet" href="css/smoothbox.css">
</head>
<body>
	
	<div class="main" id="main">
		<div id="european-main-pay" class="european-main-pay">

			<div class="hamburger">
				<a href="#" class="hamburger-item">
					<span class="hamburger-inner"></span>
				</a>
			</div>

			<div class="menu-wrap">
				
				<div class="menu-logo wow fadeInDown">
					<a href="#"><img src="images/logo.png" alt="European Insurance Company"></a>
				</div>

				<div class="menu-langs wow fadeInLeft">
					<div class="menu-langs-item">
						<a href="#">
							<img src="images/flag-ru.png" alt="ru">
							<span>ru</span>
						</a>
					</div>
					
					<div class="menu-langs-item">
						<a href="#">
							<img src="images/flag-kz.png" alt="kz">
							<span>kz</span>
						</a>
					</div>
					
					
				</div>

				<div class="menu-search wow fadeInLeft">
					<a href="#">
						<img src="images/menu-loupe.png" alt="Поиск">
					</a>
					<input placeholder="Поиск..." type="search">
				</div>

				<nav class="menu-link-wrap wow fadeInUp">
					<ul>
						<li class="<?=($_SERVER['REQUEST_URI']== '/index.php') ? 'menu-link-active' : ''?>"><a href="index.php">Главная страница</a></li>
						<li class="<?= ($_SERVER['REQUEST_URI']== '/about-us.php') ? 'menu-link-active' : ''?>"><a href="about-us.php">О компании</a></li>
						<li class="<?=($_SERVER['REQUEST_URI']== '/products.php') ? 'menu-link-active' : ''?>"><a href="products.php">Продукты</a></li>
						<li class="<?=($_SERVER['REQUEST_URI']== '/support.php') ? 'menu-link-active' : ''?>"><a href="support.php">Клиентская поддержка</a></li>
						<li class="<?=($_SERVER['REQUEST_URI']== '/ensure.php') ? 'menu-link-active' : ''?>"><a href="ensure.php">Страховой случай</a></li>
						<li class="<?=($_SERVER['REQUEST_URI']== '/pay.php') ? 'menu-link-active' : ''?>"><a href="pay.php">Оплата</a></li>
					</ul>
				</nav>

				<div class="menu-phone-number">
					<img src="images/menu-phone-number.png" class="wow shake" alt="Телефон">
					<span>+7 (727) 244 36 80</span>
				</div>

				<div class="menu-copyright">
					<p>© 2018 European Insurance Company Все права защищены.</p>
				</div>

			</div>