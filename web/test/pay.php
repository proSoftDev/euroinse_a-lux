<?php require_once('menu.php');?>

			<div class="european-pay-content">
	
				<!-- Оплата прошла успешно -->
				<div class="european-pay-successfully-wrap">
					<div class="european-pay-successfully">
						<img src="images/pay-successfully-title.png" alt="Оплата прошла успешно">
						<h5>Спасибо! Оплата прошла успешно.</h5>
						<a href="#" class="european-pay-close">Закрыть</a>
					</div>
				</div>
				<!-- End Оплата прошла успешно -->

				<!-- Оплата ошибка -->
				<div class="european-pay-mistake-wrap">
					<div class="european-pay-mistake">
						<img src="images/pay-mistake-title.png" alt="Оплата прошла успешно">
						<h4>Ошибка</h4>
						<h5>Свяжитесь с нашим консультаном</h6>

						<div class="european-pay-mistake-phone">
							<img src="images/european-mistake-phone.png" alt="Телефон">
							<h3>+7 (727) 244 36 80</h5>
						</div>

						<a href="#" class="european-pay-close">Закрыть</a>
					</div>
				</div>
				<!-- End Оплата ошибка -->


				<div class="european-pay-title wow slideInLeft">
					<h4>Оплата</h4>
					<h3>Ввести данные на оплату</h3>
				</div>

				<div class="european-pay-form-wrap">
					<form class="european-pay-form">
						<input class="wow slideInLeft" data-wow-delay="0.1s" type="text" placeholder="Номер договора">
						<input class="wow slideInLeft" data-wow-delay="0.2s" type="text" placeholder="ИИН">
						<input class="wow slideInLeft" data-wow-delay="0.3s" type="text" placeholder="Номер платежной карты">
						<input class="wow slideInLeft" data-wow-delay="0.4s" type="text" placeholder="Сумма">
						<input class="wow slideInLeft" data-wow-delay="0.45s" type="text" placeholder="Номер телефона">

						<div class="invest-agree-checkbox wow fadeInUp">
							<input id="agree-pers" type="checkbox">
							<label for="agree-pers">Даю согласие на обработку персональных данных</label>
						</div>

						<button class="wow fadeInUp">Оплатить</button>
					</form>
				</div>

			</div>
		</div>
	</div>

<?php require_once('footer.php');?>