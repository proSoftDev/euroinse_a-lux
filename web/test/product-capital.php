<?php require_once('menu.php');?>

    <div class="product-ensure-content">
        <div class="tabs">
            <ul class="nav nav-tabs ">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#6">Накопительное страхование</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" href="#2">Страхование  заемщиков</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head">Корпоративное страхование</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head">Защита семьи</a>
                </li>
            </ul>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">

            <div class="tab-pane active" id="6">
                <div class="licence">
                    <div class="inner-title wow fadeInUp">
                        <h4>Накопительное страхование</h4>
                    </div>
                    <div class="product-ensure-text wow fadeInUp">
                        <p>Страхование сотрудников организаций</p>
                        <p>Программа страхования на выбор работодателя может включать выплаты в случае:</p>
                        <ul>
                            <li> <p>Смерть;</p> </li>
                            <li> <p>Инвалидность 1, 2, 3 группы;</p></li>
                            <li> <p>Госпитализация;</p> </li>
                            <li> <p>Телесные повреждения;</p> </li>
                            <li> <p>Временная нетрудоспособность;</p> </li>
                            <li> <p>Критические заболевания;</p> </li>
                        </ul>
                        <p>Страхование действует по всему миру 24/7, независимо связан ли страховой с исполнением служебных обязанностей</p>
                        <p>Страховая сумма каждого сотрудника составляет от 1 до 10 годовых заработных плат.</p>
                    </div>
                    <div class="tab-pane" id="iur">
                        <div class="product-ensure-text">
                            <div class="inner-title">
                                <h6>Страхование сотрудников организаций</h6>
                                <div class="separator"></div>
                                <h6>Программа страхования на выбор работодателя может включать выплаты в случае:</h6>
                            </div>
                            <ul>
                                <li><p>Смерти;</p> </li>
                                <li><p>Инвалидности;</p></li>
                                <li><p>Телесных повреждениях (переломы, ожоги).</p></li>
                                <li><p>Временная нетрудоспособность;</p> </li>
                                <li><p>Критические заболевания;</p></li>
                            </ul>
                            <h6>Увеличенная в два раза выплата при наступлении страхового случая в результате ДТП.</h6>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Оплата ошибка -->



    </div>
    </div>
    </div>

<?php require_once('footer.php');?>