<?php require_once('menu.php');?>
    <div class="about-us-content">
        <div class="tabs">
            <ul class="nav nav-tabs ">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#head">Защита семьи</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab" href="#2">Страхование  заемщиков</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head">Корпоративное страхование</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head">Накопительное  страхование</a>
                </li>
            </ul>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="3">
                <div class="licence">
                    <div class="inner-title wow fadeInLeft">
                        <h4>Защита Семьи - Страхование всей семьи от несчастного случая.</h4>
                    </div>
                    <div class="separator"></div>
                    <div class="product-ensure-text wow fadeInUp space-top">
                        <div class="inner-title">
                            <h6>Программа страхования предусматривает выплаты:</h6>
                            <h6>Для Взрослых (супруг и супруга) в результате несчастного случая:</h6>
                        </div>
                        <ul>
                            <li><p>Смерти;</p> </li>
                            <li><p>Инвалидности;</p></li>
                            <li><p>Телесных повреждениях (переломы, ожоги).</p></li>
                        </ul>
                        <h6>Для детей при несчастных случаях:</h6>
                        <ul>
                            <li><p>Госпитализации;</p> </li>
                            <li><p>Телесных повреждениях (переломы, ожоги);</p></li>
                            <li><p>Телесных повреждениях (переломы, ожоги).</p></li>
                        </ul>
                        <h6>Увеличенная в два раза выплата при наступлении страхового случая в результате ДТП.</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Оплата ошибка -->



    </div>
    </div>
    </div>

<?php require_once('footer.php');?>