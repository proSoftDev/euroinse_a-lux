<?php require_once('menu.php');?>

    <div class="support-content">
        <div class="tabs wow slideInLeft">
            <ul class="nav nav-tabs ">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#contacts">Контакты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#head"> Вопросы и ответы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#licence">Словарь страховых терминов</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#bank"> Формы документов </a>
                </li>
            </ul>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active " id="contacts">
                <div class="contacts wow slideInUp">
                    <div class="contact-text">
                        <p><b>Call-центр:</b> <a href="tel:+ 7 (727) 244 36 80">+ 7 (727) 244 36 80</a></p>
                        <p><b>WhatsApp:</b><a href="tel:+ 7 (701) 084 10 28">+ 7 (701) 084 10 28</a></p>
                        <p><b>Факс:</b><a href="">+7 (727) 244 36 84</a></p>
                        <p><b>E-mail:</b><a href="">info@euroins.kz</a></p>
                        <p><b>Web сайт:</b><a href="">www.euroins.kz</a></p>
                        <p><b>Адрес:</b><a href="">Республика Казахстан, Алматы, 050059, пр-т Нурсултан Назарбаев 248<br>
                                уг.ул. Хаджимукана, БЦ Сарканд
                            </a></p>


                    </div>
                </div>
            </div>
            <div class="tab-pane " id="head">
                <div class="qa-block wow fadeInLeft" data-wow-delay="0.1s">
                    <div class="qa-section" href="#collapseExample" data-toggle="collapse">
                        Какой-то вопрос номер один?
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapseExample">
                        <div class="answer">
                            <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                    </div>
                </div>
                </div>
                <div class="qa-block wow fadeInLeft" data-wow-delay="0.2s">
                    <div class="qa-section" href="#collapseExample2" data-toggle="collapse">
                        Какой-то вопрос номер один?
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapseExample2">
                        <div class="answer">
                            <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                        </div>
                    </div>
                </div>
                <div class="qa-block wow fadeInLeft" data-wow-delay="0.3s">
                    <div class="qa-section" href="#collapseExample3" data-toggle="collapse">
                        Какой-то вопрос номер один?
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapseExample3">
                        <div class="answer">
                            <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                        </div>
                    </div>
                </div>
                <div class="qa-block wow fadeInLeft" data-wow-delay="0.4s">
                    <div class="qa-section" href="#collapseExample4" data-toggle="collapse">
                        Какой-то вопрос номер один?
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapseExample4">
                        <div class="answer">
                            <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                        </div>


                    </div>

                </div>
                <div class="more wow fadeInUp"  data-wow-delay="0.45s">
                    <p href="#collapse-all" data-toggle="collapse">Показать еще <img src="images/more-ico.png" alt=""></p>
                </div>
                <div class="collapse" id="collapse-all">
                    <div class="qa-block">
                        <div class="qa-section" href="#collapseExample5" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapseExample5">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapseExample6" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapseExample6">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapseExample7" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapseExample7">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapseExample8" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapseExample8">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="licence">
                <div class="qa-block wow fadeInRight">
                    <div class="qa-section" href="#lib" data-toggle="collapse">
                        Часто используемые термины
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="lib">
                        <div class="lib-text">
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АГЕНТ (СТРАХОВОЙ КОНСУЛЬТАНТ)    </h6>
                            </div>
                            <p>Физическое или юридическое лицо, действующее от имени Страховщика и по его поручению, в соответствии с предоставленными полномочиями.</p>
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АНДЕРРАЙТИНГ   </h6>
                            </div>
                            <p>Процедура оценки риска, в результате которой производится выявление "нестандартных рисков", требующих повышение страхового тарифа.</p>
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АКТУАРИЙ   </h6>
                            </div>
                            <p>Физическое лицо, имеющее лицензию уполномоченного органа, осуществляющее деятельность, связанную с проведением экономико-математических расчетов размеров обязательств, ставок страховых премий по договорам страхования и перестрахования, а также производящее оценку прибыльности и доходности проводимых и планируемых к проведению видов страхования страховой (перестраховочной) организации в целях обеспечения необходимого уровня платежеспособности и финансовой устойчивости страховой (перестраховочной) организации.</p>
                        </div>
                    </div>
                </div>
                <div class="qa-block wow fadeInRight" data-wow-delay="0.1s">
                    <div class="qa-section" href="#lib2" data-toggle="collapse">
                        Термины по алфавиту
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="lib2">
                        <div class="lib-text">
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АГЕНТ (СТРАХОВОЙ КОНСУЛЬТАНТ)    </h6>
                            </div>
                            <p>Физическое или юридическое лицо, действующее от имени Страховщика и по его поручению, в соответствии с предоставленными полномочиями.</p>
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АНДЕРРАЙТИНГ   </h6>
                            </div>
                            <p>Процедура оценки риска, в результате которой производится выявление "нестандартных рисков", требующих повышение страхового тарифа.</p>
                            <div class="lib-text-title">
                                <h6><img src="images/list.png" alt="">АКТУАРИЙ   </h6>
                            </div>
                            <p>Физическое лицо, имеющее лицензию уполномоченного органа, осуществляющее деятельность, связанную с проведением экономико-математических расчетов размеров обязательств, ставок страховых премий по договорам страхования и перестрахования, а также производящее оценку прибыльности и доходности проводимых и планируемых к проведению видов страхования страховой (перестраховочной) организации в целях обеспечения необходимого уровня платежеспособности и финансовой устойчивости страховой (перестраховочной) организации.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane " id="bank">
                <div class="qa-block">
                    <div class="qa-section wow slideInLeft" href="#collapse1" data-toggle="collapse">
                        Заёмщикам банка
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapse1">
                        <div class="answer-split">
                            <p>Примерные дополнительные условия к Договору добровольного страхования от <br> несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Правила добровольного страхования от несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Сактандыру жагдайынын басталуы туралы хабарлама улгісі <a href="">Скачать PDF</a></p>
                            <p>Форма УВЕДОМЛЕНИЯ о наступлении страхового случая <a href="">Скачать PDF</a></p>
                        </div>
                    </div>
                </div>
                <div class="qa-block">
                    <div class="qa-section wow slideInLeft" data-wow-delay="0.2s" href="#collapse2" data-toggle="collapse">
                        Добровольное страхование от несчастных случаев «Защита семьи»
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapse2">
                        <div class="answer-split">
                            <p>Примерные дополнительные условия к Договору добровольного страхования от <br> несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Правила добровольного страхования от несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Сактандыру жагдайынын басталуы туралы хабарлама улгісі <a href="">Скачать PDF</a></p>
                            <p>Форма УВЕДОМЛЕНИЯ о наступлении страхового случая <a href="">Скачать PDF</a></p>
                        </div>
                    </div>
                </div>
                <div class="qa-block">
                    <div class="qa-section wow slideInLeft" data-wow-delay="0.3s" href="#collapse3" data-toggle="collapse">
                        Накопительное страхование жизни
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapse3">
                        <div class="answer-split">
                            <p>Примерные дополнительные условия к Договору добровольного страхования от <br> несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Правила добровольного страхования от несчастных случаев «Защита семьи» <a href="">Скачать PDF</a></p>
                            <p>Сактандыру жагдайынын басталуы туралы хабарлама улгісі <a href="">Скачать PDF</a></p>
                            <p>Форма УВЕДОМЛЕНИЯ о наступлении страхового случая <a href="">Скачать PDF</a></p>
                        </div>
                    </div>
                </div>
                <div class="qa-block">
                    <div class="qa-section wow slideInLeft" href="#collapse4" data-wow-delay="0.4s"data-toggle="collapse">
                        Корпоративное страхование
                        <img src="images/plus-ico.png" alt="">
                    </div>
                    <div class="collapse" id="collapse4">
                        <div class="answer">
                            <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                        </div>
                    </div>
                </div>
                <div class="more wow fadeInUp" data-wow-delay="0.5s">
                    <p href="#collapse-all2" data-toggle="collapse">Показать еще <img class="img" src="images/more-ico.png" alt=""></p>
                </div>
                <div class="collapse" id="collapse-all2">
                    <div class="qa-block">
                        <div class="qa-section" href="#collapse5" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapse5">
                            <div class="answer-split">
                                <div class="left">
                                    <p>Примерные дополнительные условия к Договору добровольного страхования от несчастных случаев «Защита семьи»</p>
                                    <p>Правила добровольного страхования от несчастных случаев «Защита семьи»</p>
                                    <p>Сактандыру жагдайынын басталуы туралы хабарлама улгісі</p>
                                    <p>Форма УВЕДОМЛЕНИЯ о наступлении страхового случая</p>
                                </div>
                                <div class="right">
                                    <a href="">Скачать PDF</a>
                                    <a href="">Скачать PDF</a>
                                    <a href="">Скачать PDF</a>
                                    <a href="">Скачать PDF</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapse6" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapse6">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapse7" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapse7">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                    <div class="qa-block">
                        <div class="qa-section" href="#collapse8" data-toggle="collapse">
                            Какой-то вопрос номер один?
                            <img src="images/plus-ico.png" alt="">
                        </div>
                        <div class="collapse" id="collapse8">
                            <div class="answer">
                                <p>Таким образом укрепление и развитие структуры способствует подготовки и реализации дальнейших направлений развития. Таким образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Оплата ошибка -->



    </div>
    </div>
    </div>

<?php require_once('footer.php');?>